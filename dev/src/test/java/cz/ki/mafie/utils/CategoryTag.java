package cz.ki.mafie.utils;


public class CategoryTag {
    public static final String SLOW_TEST = "Slow_test";
    public static final String FAST_TEST = "Fast_test";
    public static final String DATABASE_TEST = "DB_test";
}

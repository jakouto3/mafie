package cz.ki.mafie.repository.log;

import cz.ki.mafie.BaseTestRunner;
import cz.ki.mafie.domain.LogRecordDO;
import cz.ki.mafie.domain.enumeration.RealestateChangeType;
import cz.ki.mafie.dto.logRecord.LogRecordDto;
import cz.ki.mafie.utils.CategoryTag;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.*;

@Tag(CategoryTag.DATABASE_TEST)
class LogRecordRepositoryTest extends BaseTestRunner {

    @Autowired
    private LogRecordRepository subject;

    @PersistenceContext
    private EntityManager em;

    @Nested
    class addLogRecord_test extends BaseTestRunner {

        @Test
        void addLogRecord_ok(){
            LogRecordDO expected = getFilledLogRecordDO();

            LogRecordDO logRecordDO = subject.addLogRecord(expected);

            LogRecordDO result = em.find(LogRecordDO.class, logRecordDO.getId());

            assertEquals(expected.getRealEstateName(), result.getRealEstateName());
            assertEquals(expected.getRealEstateNum(), result.getRealEstateNum());
            assertEquals(expected.getRecordedTime(), result.getRecordedTime());
            assertEquals(expected.getTeamLabel(), result.getTeamLabel());
            assertEquals(expected.getRealestateChangeType(), result.getRealestateChangeType());
        }
    }

    private LogRecordDO getFilledLogRecordDO() {
        return LogRecordDO.builder()
                    .realEstateName("testLogRecord")
                    .realEstateNum(999)
                    .recordedTime(LocalDateTime.MAX)
                    .teamLabel("testLogRecord")
                    .realestateChangeType(RealestateChangeType.CLAIM)
                .build();
    }
}
package cz.ki.mafie.repository.log;

import cz.ki.mafie.BaseTestRunner;
import cz.ki.mafie.domain.LogRecordDO;
import cz.ki.mafie.domain.enumeration.RealestateChangeType;
import cz.ki.mafie.exception.MessageException;
import cz.ki.mafie.utils.CategoryTag;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
@Tag(CategoryTag.FAST_TEST)
class LogRecordRepositoryImplTest extends BaseTestRunner{

    @InjectMocks
    private LogRecordRepositoryImpl subject;

    @PersistenceContext
    private EntityManager em;

    @Nested
    class validate_tests extends BaseTestRunner{

        LogRecordDO logRecordDO;

        @BeforeEach
        void init(){
            logRecordDO = createRecordLogDO();
        }

        @Test
        void correctParameters_ok(){
            subject.validate(logRecordDO);
        }

        @Test
        void nullRealestateName_throwME(){
            logRecordDO.setRealEstateName(null);

            assertThrows(RuntimeException.class, () -> subject.validate(logRecordDO));
        }

        @Test
        void emptyRealestateName_throwME(){
            logRecordDO.setRealEstateName("");

            assertThrows(RuntimeException.class, () -> subject.validate(logRecordDO));
        }

        @Test
        void nullRecordedTime_throwNLP(){
            logRecordDO.setRecordedTime(null);

            assertThrows(NullPointerException.class, () -> subject.validate(logRecordDO));
        }

        @Test
        void nullTeamLabel_throwME(){
            logRecordDO.setTeamLabel(null);

            assertThrows(RuntimeException.class, () -> subject.validate(logRecordDO));
        }

        @Test
        void emptyTeamLabel_throwME(){
            logRecordDO.setTeamLabel("");

            assertThrows(RuntimeException.class, () -> subject.validate(logRecordDO));
        }

        @Test
        void nullChangeType_throwNLP(){
            logRecordDO.setRealestateChangeType(null);

            assertThrows(NullPointerException.class, () -> subject.validate(logRecordDO));
        }

        @Test
        void negativeRealestateNumber_throwME(){
            logRecordDO.setRealEstateNum(-1);

            assertThrows(MessageException.class, () -> subject.validate(logRecordDO));
        }
    }

    private LogRecordDO createRecordLogDO(){
        return LogRecordDO.builder()
                .realestateChangeType(RealestateChangeType.BURN)
                .realEstateName("realestate")
                .realEstateNum(1)
                .recordedTime(LocalDateTime.now())
                .teamLabel("team")
                .build();
    }
}
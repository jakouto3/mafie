package cz.ki.mafie.repository.realestate;

import cz.ki.mafie.BaseTestRunner;
import cz.ki.mafie.domain.RealestateDO;
import cz.ki.mafie.dto.realestate.RealestateDto;
import cz.ki.mafie.exception.MessageException;
import cz.ki.mafie.utils.CategoryTag;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.InvalidDataAccessApiUsageException;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@Tag(CategoryTag.DATABASE_TEST)
class RealestateRepositoryTest extends BaseTestRunner {

    @PersistenceContext
    private EntityManager em;

    @Autowired
    private RealestateRepository subject;

    @Nested
    class findById_tests extends BaseTestRunner{
        @Test
        void realEstateExists_found(){
            RealestateDO expected = new RealestateDO();

            em.persist(expected);

            Optional<RealestateDO> result = subject.findById(expected.getId());

            assertTrue(result.isPresent());
            assertEquals(expected, result.get());
        }

        @Test
        void nullId_throwIDAAUE(){
            assertThrows(InvalidDataAccessApiUsageException.class, () -> subject.findById(null));
        }

        @Test
        void realEstatePersisted_searchDifferentId_notFound(){
            RealestateDO expected = new RealestateDO();

            em.persist(expected);

            Optional<RealestateDO> result = subject.findById(expected.getId() + 1);

            assertFalse(result.isPresent());
        }
    }

    @Nested
    class validate_tests extends BaseTestRunner{

        RealestateDO reDO;

        @BeforeEach
        void init(){
            reDO = getFilledReDO();
        }

        @Test
        void validate_noExc(){
            subject.validate(reDO);
        }

        @Test
        void priceIsNull_throwsME(){
            reDO.setPrice(null);

            assertThrows(MessageException.class,()->subject.validate(reDO));
        }

        @Test
        void priceIsNegative_throwsME(){
            reDO.setPrice(-1);

            assertThrows(MessageException.class,()->subject.validate(reDO));
        }

        @Test
        void profitPerMinuteIsNull_throwsME(){
            reDO.setProfitPerMinute(null);

            assertThrows(MessageException.class,()->subject.validate(reDO));
        }

        @Test
        void profitPerMinuteIsNegative_throwsME(){
            reDO.setProfitPerMinute(-1);

            assertThrows(MessageException.class,()->subject.validate(reDO));
        }

        @Test
        void shapeIsNull_throwsME(){
            reDO.setShape(null);

            assertThrows(MessageException.class,()->subject.validate(reDO));
        }

        @Test
        void shapeIsEmpty_throwsME(){
            reDO.setShape("");

            assertThrows(MessageException.class,()->subject.validate(reDO));
        }

        @Test
        void nameIsNull_throwsME(){
            reDO.setName(null);

            assertThrows(MessageException.class,()->subject.validate(reDO));
        }

        @Test
        void nameIsEmpty_throwsME(){
            reDO.setName("");

            assertThrows(MessageException.class,()->subject.validate(reDO));
        }

        @Test
        void districtNumberNegative_throwsME(){
            reDO.setDistrictNumber(-1);

            assertThrows(MessageException.class,()->subject.validate(reDO));
        }

        @Test
        void districtNumberNull_throwsME(){
            reDO.setDistrictNumber(null);

            assertThrows(MessageException.class,()->subject.validate(reDO));
        }
    }

    private RealestateDO getFilledReDO(){
        return RealestateDO.builder()
                .name("test")
                .districtNumber(1)
                .price(400)
                .profitPerMinute(400)
                .shape("test")
                .build();
    }
}
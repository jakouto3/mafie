package cz.ki.mafie.repository.realestateHistoryRecord;

import cz.ki.mafie.BaseTestRunner;
import cz.ki.mafie.domain.ProfileDO;
import cz.ki.mafie.domain.RealestateDO;
import cz.ki.mafie.domain.RealestateHistoryRecordDO;
import cz.ki.mafie.domain.TeamProfileDO;
import cz.ki.mafie.domain.enumeration.RealestateChangeType;
import cz.ki.mafie.dto.realestateHistoryRecord.ReHistoryRecordDto;
import cz.ki.mafie.exception.MessageException;
import cz.ki.mafie.utils.CategoryTag;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.time.LocalDateTime;
import java.time.Period;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.when;

@Tag(CategoryTag.DATABASE_TEST)
class RealestateHistoryRecordRepositoryTest extends BaseTestRunner{

    @Autowired
    private RealestateHistoryRecordRepository subject;

    @PersistenceContext
    private EntityManager em;

    @Nested
    class validate_tests extends BaseTestRunner{

        TeamProfileDO owner;
        ProfileDO author;
        RealestateDO re;
        RealestateHistoryRecordDO recordDO;

        @BeforeEach
        void init(){
            owner = createOwner();
            em.persist(owner);

            author = createAuthor();
            em.persist(author);

            re = createRealestate(400,1);
            em.persist(re);

            recordDO = createRecordDO(owner,re,author);
            em.persist(recordDO);
        }

        @Test
        void profitPerMinuteNegative_throwME(){
            recordDO.setProfitPerMinute(-1);
            assertThrows(MessageException.class,
                    () -> subject.validate(recordDO));
        }

        @Test
        void timeFromNull_throwNLP(){
            recordDO.setTimeFrom(null);
            assertThrows(NullPointerException.class,
                    () -> subject.validate(recordDO));
        }

        @Test
        void timeToLessThanTimeFrom_throwME(){
            recordDO.setTimeTo(recordDO.getTimeFrom());
            recordDO.setTimeFrom(recordDO.getTimeFrom().plus(Period.ofDays(1)));
            assertThrows(MessageException.class,
                    () -> subject.validate(recordDO));
        }

        @Test
        void realestateIsNull_throwNLP(){
            recordDO.setRealestate(null);
            assertThrows(NullPointerException.class,
                    () -> subject.validate(recordDO));
        }

        @Test
        void actionTypeIsNull_throwNLP(){
            recordDO.setRealestateChangeType(null);
            assertThrows(NullPointerException.class,
                    () -> subject.validate(recordDO));
        }

        @Test
        void authorIsNull_throwNLP(){
            recordDO.setAuthor(null);
            assertThrows(NullPointerException.class,
                    () -> subject.validate(recordDO));
        }

        @Test
        void validate_noExc(){
            subject.validate(recordDO);
        }
    }

    @Nested
    class validateReRecordsTimeConsequence_tests extends BaseTestRunner{

        TeamProfileDO owner;
        ProfileDO author;
        RealestateDO re;
        RealestateHistoryRecordDO recordDO1;
        RealestateHistoryRecordDO recordDO2;

        @BeforeEach
        void init(){
            owner = createOwner();
            em.persist(owner);

            author = createAuthor();
            em.persist(author);

            re = createRealestate(400,1);
            em.persist(re);

            recordDO1 = createRecordDO(owner,re,author);
            recordDO1.setTimeFrom(recordDO1.getTimeFrom().minus(Period.ofDays(1)));
            recordDO1.setTimeTo(recordDO1.getTimeFrom().plus(Period.ofDays(3)));
            em.persist(recordDO1);

            recordDO2 = createRecordDO(owner,re,author);
            recordDO2.setTimeFrom(recordDO1.getTimeTo());
            recordDO2.setTimeTo(null);
            em.persist(recordDO2);
        }

        @Test
        void validateTimeConsequence_ok(){
            subject.validateReRecordsTimeConsequence(re.getId());
        }

        @Test
        void validateTimeConsequenceEmptyRecords_ok(){
            subject.validateReRecordsTimeConsequence(1);
        }

        @Test
        void validateTimeConsequenceHoleInconsistency_throwIAE(){
            recordDO2.setTimeFrom(recordDO2.getTimeFrom().plus(Period.ofDays(3)));
            assertThrows(RuntimeException.class, () -> subject.validateReRecordsTimeConsequence(re.getId()));
        }

        @Test
        void validateTimeConsequenceOverlapInconsistency_throwIAE(){
            recordDO2.setTimeFrom(recordDO1.getTimeTo().minus(Period.ofDays(1)));
            assertThrows(RuntimeException.class, () -> subject.validateReRecordsTimeConsequence(re.getId()));
        }

        @Test
        void validateTimeConsequenceLastRecordsTimeToNotNull_throwIAE(){
            recordDO2.setTimeTo(LocalDateTime.now());
            assertThrows(RuntimeException.class, () -> subject.validateReRecordsTimeConsequence(re.getId()));
        }
    }


    //TODO - these test methods are testing same problems as validate and update, we should mock these two methods
    //TODO since validate is method of this class we dont need to mock
    @Nested
    class create_tests extends BaseTestRunner{

        RealestateHistoryRecordDO recordDO;
        TeamProfileDO owner;
        ProfileDO author;
        RealestateDO re;

        @BeforeEach
        void init(){

            owner = createOwner();
            em.persist(owner);
            author = createAuthor();
            em.persist(author);
            re = createRealestate(400,1);
            em.persist(re);

            recordDO = createRecordDO(owner,re,author);
        }

        @Test
        void create_noExc(){
            subject.create(recordDO);
        }

        @Test
        void createWithNullOwner_ok(){
            recordDO.setOwner(null);
            subject.create(recordDO);
        }

    }

    @Nested
    class findAllByRealestate_IdOrderByTimeFromAsc_tests extends BaseTestRunner{

        TeamProfileDO owner;
        ProfileDO author;
        RealestateDO re;

        @BeforeEach
        void init(){
            owner = createOwner();
            em.persist(owner);
            author = createAuthor();
            em.persist(author);
            re = createRealestate(400,1);
            em.persist(re);
        }

        @Test
        void oneRecord_ok(){
            RealestateHistoryRecordDO do1 = createRecordDO(owner,re,author);
            em.persist(do1);

            List<RealestateHistoryRecordDO> result = subject.findAllByRealestate_IdOrderByTimeFromAsc(re.getId());

            assertNotNull(result);
            assertNotNull(result.get(0));
            assertEquals(result.get(0),do1);
        }

        @Test
        void twoRecordsSwitched_ok(){
            RealestateHistoryRecordDO do1 = createRecordDO(owner,re,author);
            em.persist(do1);
            RealestateHistoryRecordDO do2 = createRecordDO(owner,re,author);
            em.persist(do2);

            LocalDateTime tmp = LocalDateTime.now();
            do2.setTimeFrom(tmp.minus(Period.ofDays(1)));
            do2.setTimeTo(tmp);
            do1.setTimeFrom(tmp);
            do1.setTimeTo(tmp.plus(Period.ofDays(1)));


            List<RealestateHistoryRecordDO> result = subject.findAllByRealestate_IdOrderByTimeFromAsc(re.getId());
            assertNotNull(result);

            assertNotNull(result.get(1));
            assertEquals(result.get(1),do1);

            assertNotNull(result.get(0));
            assertEquals(result.get(0),do2);
        }

        @Test
        void empty_ok(){
            List<RealestateHistoryRecordDO> result = subject.findAllByRealestate_IdOrderByTimeFromAsc(re.getId());
            assertNotNull(result);
            assertTrue(result.isEmpty());
        }
    }

    @Nested
    class findLastRecordForRe_tests extends BaseTestRunner{
        TeamProfileDO owner;
        ProfileDO author;
        RealestateDO re;

        @BeforeEach
        void init(){
            owner = createOwner();
            em.persist(owner);
            author = createAuthor();
            em.persist(author);
            re = createRealestate(5000,1);
            em.persist(re);
        }


        @Test
        void oneItem_ok(){
            RealestateHistoryRecordDO do1 = createRecordDO(owner,re,author);
            em.persist(do1);

            Optional<RealestateHistoryRecordDO> result = subject.findLastRecordForRe(re.getId());

            assertTrue(result.isPresent());
            assertEquals(do1,result.get());
        }

        @Test
        void twoItems_ok(){
            RealestateHistoryRecordDO do1 = createRecordDO(owner,re,author);
            em.persist(do1);
            RealestateHistoryRecordDO do2 = createRecordDO(owner,re,author);
            em.persist(do2);

            Optional<RealestateHistoryRecordDO> result = subject.findLastRecordForRe(re.getId());

            assertTrue(result.isPresent());
            assertEquals(do2,result.get());
        }

        @Test
        void emptyList_ok(){
            Optional<RealestateHistoryRecordDO> result = subject.findLastRecordForRe(re.getId());

            assertEquals(Optional.empty(),result);
        }

    }

    private TeamProfileDO createOwner(){
        return TeamProfileDO.teamProfileBuilder()
                .build();
    }

    private RealestateDO createRealestate(int profitPerMinute,int distNum) {
        return  RealestateDO.builder()
                .profitPerMinute(profitPerMinute)
                .districtNumber(distNum)
                .price(422)
                .build();
    }

    private ProfileDO createAuthor(){
        return ProfileDO.profileBuilder()
                .build();
    }

    private RealestateHistoryRecordDO createRecordDO(TeamProfileDO owner, RealestateDO re, ProfileDO author){
        return RealestateHistoryRecordDO.builder()
                .author(author)
                .owner(owner)
                .realestate(re)
                .timeFrom(LocalDateTime.now())
                .timeTo(null)
                .profitPerMinute(400)
                .realestateChangeType(RealestateChangeType.BURN)
                .build();
    }

    private ReHistoryRecordDto createRecordDTO(int owner, int author, int re){
        return ReHistoryRecordDto.builder()
                .authorId(author)
                .ownerId(owner)
                .realestateId(re)
                .profitPerMinute(500)
                .timeFrom(LocalDateTime.now())
                .timeTo(LocalDateTime.now())
                .actionType(RealestateChangeType.CLAIM)
                .build();
    }


}

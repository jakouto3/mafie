package cz.ki.mafie.repository.generalProfile;

import cz.ki.mafie.BaseTestRunner;
import cz.ki.mafie.domain.GeneralProfileDO;
import cz.ki.mafie.domain.RealestateDO;
import cz.ki.mafie.utils.CategoryTag;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static cz.ki.mafie.repository.generalProfile.GeneralProfilePredicates.generalProfileLocatedIn;
import static org.junit.jupiter.api.Assertions.*;

@Tag(CategoryTag.DATABASE_TEST)
class GeneralProfileRepositoryTest extends BaseTestRunner {

    @Autowired
    private GeneralProfileRepository subject;

    @PersistenceContext
    private EntityManager em;

    /**
     * WARNING - class must extend BaseTestRunner to be able to correctly load ApplicationContext
     * */
    @Nested
    class getProfilesLocatedInRe_tests extends BaseTestRunner{
        private List<GeneralProfileDO> generateGeneralProfiles(int num, RealestateDO re){
            return IntStream.range(0, num).mapToObj(i ->
                    GeneralProfileDO.generalProfileBuilder()
                            .description("ProfileDescription@" + String.valueOf(i))
                            .realestate(re)
                            .build())
                    .collect(Collectors.toList());
        }

        @Test
        void findAllProfiles_ok(){
            RealestateDO realestate = createRealestate(500,1);
            em.persist(realestate);

            List<GeneralProfileDO> profiles = generateGeneralProfiles(10, realestate);
            profiles.forEach(p -> em.persist(p));
            List<GeneralProfileDO> redundantProfiles = generateGeneralProfiles(10, null);
            redundantProfiles.forEach(p -> em.persist(p));

            List<GeneralProfileDO> result = subject.getProfilesLocatedInRe(realestate.getId());
            List<GeneralProfileDO> resultJpaRepo = subject.findAllByRealestate_Id(realestate.getId());
            List<GeneralProfileDO> resultQueryDslPred = subject.findAllToList(generalProfileLocatedIn(realestate.getId()));

            assertEquals(profiles.size(), result.size());
            assertEquals(profiles.size(), resultJpaRepo.size());
            assertEquals(profiles.size(), resultQueryDslPred.size());

            profiles.sort(Comparator.comparingInt(GeneralProfileDO::getId));
            result.sort(Comparator.comparingInt(GeneralProfileDO::getId));
            resultJpaRepo.sort(Comparator.comparingInt(GeneralProfileDO::getId));
            resultQueryDslPred.sort(Comparator.comparingInt(GeneralProfileDO::getId));

            assertArrayEquals(profiles.toArray(), result.toArray());
            assertArrayEquals(profiles.toArray(), resultJpaRepo.toArray());
            assertArrayEquals(profiles.toArray(), resultQueryDslPred.toArray());
        }

        @Test
        void findNoProfile_ok(){
            RealestateDO realestate =  createRealestate(500,1);
            em.persist(realestate);

            List<GeneralProfileDO> redundantProfiles = generateGeneralProfiles(10, null);
            redundantProfiles.forEach(p -> em.persist(p));

            List<GeneralProfileDO> result = subject.getProfilesLocatedInRe(realestate.getId());
            List<GeneralProfileDO> resultJpaRepo = subject.findAllByRealestate_Id(realestate.getId());
            List<GeneralProfileDO> resultQueryDslPred = subject.findAllToList(generalProfileLocatedIn(realestate.getId()));

            assertEquals(0, result.size());
            assertEquals(0, resultJpaRepo.size());
            assertEquals(0, resultQueryDslPred.size());
        }

        private RealestateDO createRealestate(int profitPerMinute,int distNum) {
            return  RealestateDO.builder()
                    .profitPerMinute(profitPerMinute)
                    .districtNumber(distNum)
                    .price(422)
                    .build();
        }
    }
}
package cz.ki.mafie.service;

import cz.ki.mafie.BaseTestRunner;
import cz.ki.mafie.domain.*;
import cz.ki.mafie.domain.enumeration.RealestateChangeType;
import cz.ki.mafie.domain.enumeration.SystemRole;
import cz.ki.mafie.dto.UserDto;
import cz.ki.mafie.dto.realestate.ReActionDto;
import cz.ki.mafie.dto.realestate.RealestateDto;
import cz.ki.mafie.dto.realestateHistoryRecord.ReHistoryRecordInfoDto;
import cz.ki.mafie.exception.MessageException;
import cz.ki.mafie.repository.generalProfile.GeneralProfileRepository;
import cz.ki.mafie.repository.log.LogRecordRepository;
import cz.ki.mafie.repository.profile.ProfileRepository;
import cz.ki.mafie.repository.realestate.RealestateRepository;
import cz.ki.mafie.repository.realestateHistoryRecord.RealestateHistoryRecordRepository;
import cz.ki.mafie.repository.teamProfile.TeamProfileRepository;
import cz.ki.mafie.repository.user.UserRepository;
import cz.ki.mafie.service.converter.HistoryRecordConverter;
import cz.ki.mafie.service.converter.RealestateConverter;
import cz.ki.mafie.service.impl.RealestateServiceImpl;
import cz.ki.mafie.utils.CategoryTag;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
@Tag(CategoryTag.FAST_TEST)
class RealestateServiceTest extends BaseTestRunner {

    @InjectMocks
    private RealestateServiceImpl subject;

    @Mock
    private RealestateRepository reRepository;

    @Mock
    private LogRecordRepository logRepo;

    @Mock
    private RealestateHistoryRecordRepository reHistoryRepo;

    @Mock
    private GeneralProfileRepository genProfRepo;

    @Mock
    private ProfileRepository profileRepo;

    @Mock
    private TeamProfileRepository teamProfRepo;

    @Mock
    private UserRepository userRepo;

    private final UserDto LOGGED_USER_DTO = UserDto.builder()
            .id(123)
            .roles(SystemRole.GAMEMASTER)
            .username("TEST_LOGGED_USER_USERNAME")
            .build();

    private final UserDO LOGGED_USER = UserDO.userBuilder()
            .id(123)
            .systemRole(SystemRole.GAMEMASTER)
            .login("TEST_LOGGED_USER_USERNAME")
            .build();

    @Nested
    class CanBeClaimed_tests extends BaseTestRunner{

        @Test
        void lastRecordIsBurn_ok () {
            int reId = 50;
            RealestateDO realestate = generateRealestates(1).get(0);
            ProfileDO profile = generateProfiles(1,realestate).get(0);
            TeamProfileDO team = generateTeams(1,realestate).get(0);
            List<RealestateHistoryRecordDO> histRecords = generateReHistRecords(5, realestate, profile, team, RealestateChangeType.BURN, RealestateChangeType.CLAIM);

            when(reHistoryRepo.findLastNonChangeProfitRecord(reId)).thenReturn(findLastNonChangeProfitRecord(histRecords));

            boolean result = subject.canBeClaimed(reId);

            assertTrue(result);
        }

        @Test
        void lastRecordIsClaimed_ok () {
            int reId = 50;
            RealestateDO realestate = generateRealestates(1).get(0);
            ProfileDO profile = generateProfiles(1,realestate).get(0);
            TeamProfileDO team = generateTeams(1,realestate).get(0);
            List<RealestateHistoryRecordDO> histRecords = generateReHistRecords(5, realestate, profile, team, RealestateChangeType.CLAIM, RealestateChangeType.CLAIM);

            when(reHistoryRepo.findLastNonChangeProfitRecord(reId)).thenReturn(findLastNonChangeProfitRecord(histRecords));

            boolean result = subject.canBeClaimed(reId);

            assertFalse(result);
        }
    }

    @Nested
    class CanBeBurned_tests extends BaseTestRunner{

        @Test
        void lastRecordIsBurn_ok () {
            int reId = 50;
            RealestateDO realestate = generateRealestates(1).get(0);
            ProfileDO profile = generateProfiles(1,realestate).get(0);
            TeamProfileDO team = generateTeams(1,realestate).get(0);
            List<RealestateHistoryRecordDO> histRecords = generateReHistRecords(5, realestate, profile, team, RealestateChangeType.BURN, RealestateChangeType.CLAIM);

            when(reHistoryRepo.findLastNonChangeProfitRecord(reId)).thenReturn(findLastNonChangeProfitRecord(histRecords));

            boolean result = subject.canBeBurned(reId);

            assertFalse(result);
        }

        @Test
        void lastRecordIsClaimed_ok () {
            int reId = 50;
            RealestateDO realestate = generateRealestates(1).get(0);
            ProfileDO profile = generateProfiles(1,realestate).get(0);
            TeamProfileDO team = generateTeams(1,realestate).get(0);
            List<RealestateHistoryRecordDO> histRecords = generateReHistRecords(5, realestate, profile, team, RealestateChangeType.CLAIM, RealestateChangeType.CLAIM);

            when(reHistoryRepo.findLastNonChangeProfitRecord(reId)).thenReturn(findLastNonChangeProfitRecord(histRecords));

            boolean result = subject.canBeBurned(reId);

            assertTrue(result);
        }
    }

    @Nested
    class CanDeleteRecord_tests extends BaseTestRunner{

        @Test
        void lastRecord_ok () {
            int num = 5;
            int recordId = 4;
            int reId = 0;
            RealestateDO realestate = generateRealestates(1).get(0);
            ProfileDO profile = generateProfiles(1,realestate).get(0);
            TeamProfileDO team = generateTeams(1,realestate).get(0);
            List<RealestateHistoryRecordDO> histRecords = generateReHistRecords(num, realestate, profile, team, RealestateChangeType.BURN, RealestateChangeType.CLAIM);

            when(reHistoryRepo.findById(recordId)).thenReturn(Optional.of(histRecords.get(recordId)));
            when(reHistoryRepo.findLastRecordForRe(reId)).thenReturn(Optional.of(histRecords.get(recordId)));

            boolean result = subject.canDeleteRecord(recordId);

            assertTrue(result);
        }

        @Test
        void notLastRecord_ok () {
            int num = 5;
            int recordId = 3;
            int lastRecordId = 4;
            int reId = 0;
            RealestateDO realestate = generateRealestates(1).get(0);
            ProfileDO profile = generateProfiles(1,realestate).get(0);
            TeamProfileDO team = generateTeams(1,realestate).get(0);
            List<RealestateHistoryRecordDO> histRecords = generateReHistRecords(num, realestate, profile, team, RealestateChangeType.BURN, RealestateChangeType.CLAIM);

            when(reHistoryRepo.findById(recordId)).thenReturn(Optional.of(histRecords.get(recordId)));
            when(reHistoryRepo.findLastRecordForRe(reId)).thenReturn(Optional.of(histRecords.get(lastRecordId)));

            boolean result = subject.canDeleteRecord(recordId);

            assertFalse(result);
        }
    }

    @Nested
    class DeleteRecord_tests extends BaseTestRunner{

        @Test
        void lastRecord_ok () {
            int num = 5;
            int recordId = 4;
            int reId = 0;
            RealestateDO realestate = generateRealestates(1).get(0);
            ProfileDO profile = generateProfiles(1,realestate).get(0);
            TeamProfileDO team = generateTeams(1,realestate).get(0);
            List<RealestateHistoryRecordDO> histRecords = generateReHistRecords(num, realestate, profile, team, RealestateChangeType.BURN, RealestateChangeType.CLAIM);
            RealestateHistoryRecordDO toDelete = histRecords.get(recordId);

            when(reHistoryRepo.findAllByRealestate_IdOrderByTimeFromAsc(reId)).thenReturn(histRecords);
            when(reHistoryRepo.findById(recordId)).thenReturn(Optional.of(histRecords.get(recordId)));
            doAnswer(invocation -> null).when(reHistoryRepo).deleteById(recordId);
            doAnswer(invocation -> {
                histRecords.remove(toDelete);//todo here you delete
                return null;
            }).when(reHistoryRepo).switchToLastRecord(histRecords.get(3));

            subject.deleteRecord(recordId);

            //todo here you check - this is really strange
            assertFalse(histRecords.contains(toDelete));
        }

        @Test
        void notExistingRecord_throwExc (){
            int num = 5;
            int recordId = 4;
            int reId = 0;
            int notExistingRecordId = 25;
            RealestateDO realestate = generateRealestates(1).get(0);
            ProfileDO profile = generateProfiles(1,realestate).get(0);
            TeamProfileDO team = generateTeams(1,realestate).get(0);
            List<RealestateHistoryRecordDO> histRecords = generateReHistRecords(num, realestate, profile, team, RealestateChangeType.BURN, RealestateChangeType.CLAIM);
            RealestateHistoryRecordDO toDelete = histRecords.get(recordId);

            when(reHistoryRepo.findAllByRealestate_IdOrderByTimeFromAsc(reId)).thenReturn(histRecords);
            when(reHistoryRepo.findById(recordId)).thenReturn(Optional.of(histRecords.get(recordId)));
            doAnswer(invocation -> null).when(reHistoryRepo).deleteById(recordId);
            doAnswer(invocation -> {
                histRecords.remove(toDelete);
                return null;
            }).when(reHistoryRepo).switchToLastRecord(histRecords.get(3));

            assertThrows(RuntimeException.class, () -> subject.deleteRecord(notExistingRecordId));
        }

        @Test
        void notLastRecord_throwExc() {
            int num = 5;
            int reId = 0;
            int notLastRecordId = 2;
            RealestateDO realestate = generateRealestates(1).get(0);
            ProfileDO profile = generateProfiles(1,realestate).get(0);
            TeamProfileDO team = generateTeams(1,realestate).get(0);
            List<RealestateHistoryRecordDO> histRecords = generateReHistRecords(num, realestate, profile, team, RealestateChangeType.BURN, RealestateChangeType.CLAIM);

            when(reHistoryRepo.findAllByRealestate_IdOrderByTimeFromAsc(reId)).thenReturn(histRecords);
            when(reHistoryRepo.findById(notLastRecordId)).thenReturn(Optional.of(histRecords.get(notLastRecordId)));

            assertThrows(MessageException.class, () -> subject.deleteRecord(notLastRecordId));
        }
    }

    @Nested
    class GetReDetail_tests extends BaseTestRunner{

        @Test
        void existingRealEstateWithNonEmptyHistory_ok () {
            int num = 5;
            int reId = 0;
            RealestateDO realestate = generateRealestates(1).get(0);
            ProfileDO profile = generateProfiles(1,realestate).get(0);
            TeamProfileDO team = generateTeams(1,realestate).get(0);
            List<RealestateHistoryRecordDO> histRecords = generateReHistRecords(num, realestate, profile, team, RealestateChangeType.BURN, RealestateChangeType.CLAIM);

            when(reHistoryRepo.findAllByRealestate_IdOrderByTimeFromAsc(reId)).thenReturn(histRecords);
            when(reRepository.findById(reId)).thenReturn(Optional.of(realestate));


            List<ReHistoryRecordInfoDto> expected = histRecords.stream()
                    .map(HistoryRecordConverter::toInfoDto)
                    .collect(Collectors.toList());

            List<ReHistoryRecordInfoDto> result = subject.getReDetail(reId).getReHistoryRecords();

            assertEquals(expected.size(), result.size());
            for(int i=0; i<expected.size(); i++){
                assertEquals(expected.get(i).getCreatorName(), result.get(i).getCreatorName());
                assertEquals(expected.get(i).getTeamLabel(), result.get(i).getTeamLabel());
                assertEquals(expected.get(i).getReHistoryRecordDto(), result.get(i).getReHistoryRecordDto());
            }
        }

        @Test
        void existingRealEstateWithEmptyHistory_ok () {
            int reId = 0;
            RealestateDO realestate = generateRealestates(1).get(0);

            when(reHistoryRepo.findAllByRealestate_IdOrderByTimeFromAsc(reId)).thenReturn(new ArrayList<>());
            when(reRepository.findById(reId)).thenReturn(Optional.of(realestate));

            List<ReHistoryRecordInfoDto> expected = new ArrayList<>();

            List<ReHistoryRecordInfoDto> result = subject.getReDetail(reId).getReHistoryRecords();

            assertEquals(expected.size(), result.size());
        }

        @Test
        void nonExistingRealEstate_throwExc () {
            int reId = 0;

            when(reRepository.findById(reId)).thenReturn(Optional.empty());

            assertThrows(RuntimeException.class, () -> subject.getReDetail(reId));
        }
    }

    @Nested
    class UpdateReInfo_tests extends BaseTestRunner{

        @Test
        void existingRealEstateNotUpdatingProfit_ok () {
            int reId = 0;
            RealestateDO realestate = generateRealestates(1).get(0);

            RealestateDto realestateDto = RealestateConverter.toDto(realestate, Optional.empty());
            realestateDto.setName("ChangedName");


            when(reRepository.findById(reId)).thenReturn(Optional.of(realestate));

            //TODO mělo by vracet ReDetailOverviewDto aby se nemusel pouzivat conventor a jen se testovala vracena hodnota
            subject.updateReInfo(realestateDto, LOGGED_USER_DTO);
            RealestateDto result = RealestateConverter.toDto(realestate, Optional.empty());

            assertEquals(realestateDto.getId(),result.getId());
            assertEquals(realestateDto.getDistrictNumber(),result.getDistrictNumber());
            assertEquals(realestateDto.getProfitPerMinute(),result.getProfitPerMinute());
            assertEquals(realestateDto.getPrice(),result.getPrice());
            assertEquals(realestateDto.getName(),result.getName());
            assertEquals(realestateDto.getShape(),result.getShape());
        }

        @Test
        void existingRealEstateUpdatingProfit_ok () {
            Integer expected = 5;
            int reId = 0;
            int recordId = 4;
            int num = 5;
            RealestateDO realestate = generateRealestates(1).get(0);
            TeamProfileDO team = generateTeams(1,realestate).get(0);
            List<RealestateHistoryRecordDO> histRecords = generateReHistRecords(num, realestate, LOGGED_USER, team, RealestateChangeType.BURN, RealestateChangeType.CLAIM);

            RealestateDto realestateDto = RealestateConverter.toDto(realestate, Optional.empty());
            realestateDto.setName("ChangedName");
            realestateDto.setProfitPerMinute(expected);

            RealestateHistoryRecordDO resultHist = new RealestateHistoryRecordDO();
            LogRecordDO resultLog = new LogRecordDO();

            when(reRepository.findById(reId)).thenReturn(Optional.of(realestate));
            when(teamProfRepo.findById(team.getId())).thenReturn(Optional.of(team));
            when(reHistoryRepo.findLastNonChangeProfitRecord(realestate.getId())).thenReturn(findLastNonChangeProfitRecord(histRecords));
            when(userRepo.findById(LOGGED_USER.getId())).thenReturn(Optional.of(LOGGED_USER));

            doAnswer(invocation -> {
                resultHist.setProfitPerMinute(expected);
                return null;
            }).when(reHistoryRepo).create(any(RealestateHistoryRecordDO.class));

            when(reHistoryRepo.findLastRecordForRe(reId)).thenReturn(Optional.of(histRecords.get(recordId)));
            when(reRepository.findById(reId)).thenReturn(Optional.of(realestate));
            doAnswer(invocation -> null).when(reRepository).validate(any(RealestateDO.class));

            subject.updateReInfo(realestateDto, LOGGED_USER_DTO);
            RealestateDto result = RealestateConverter.toDto(realestate, Optional.empty());

            assertEquals(realestateDto.getId(),result.getId());
            assertEquals(realestateDto.getDistrictNumber(),result.getDistrictNumber());
            assertEquals(realestateDto.getProfitPerMinute(),result.getProfitPerMinute());
            assertEquals(realestateDto.getPrice(),result.getPrice());
            assertEquals(realestateDto.getName(),result.getName());
            assertEquals(realestateDto.getShape(),result.getShape());

            assertEquals(expected,resultHist.getProfitPerMinute());
            assertNotEquals(expected,resultLog.getRealEstateNum());
        }

        @Test
        void notExistingRealEstate_throwExc () {
            int reId = 0;
            int notExistingReId = 50;
            RealestateDO realestate = generateRealestates(1).get(0);

            RealestateDto realestateDto = RealestateConverter.toDto(realestate, Optional.empty());
            realestateDto.setId(notExistingReId);

            when(reRepository.findById(reId)).thenReturn(Optional.empty());

            assertThrows(RuntimeException.class, () -> subject.updateReInfo(realestateDto, LOGGED_USER_DTO));
        }
    }

    @Nested
    class CreateRecord_tests extends BaseTestRunner{

        @Test
        void notExistingOwner_throwExc () {
            int reId = 0;
            int nonExistingOwnerId = 50;

            ReActionDto reActionDto = generateReActionDto(reId, RealestateChangeType.CLAIM, LOGGED_USER.getId(), nonExistingOwnerId);

            assertThrows(RuntimeException.class, () -> subject.createRecord(reActionDto, LOGGED_USER_DTO));
        }

        @Test
        void notExistingAuthor_throwExc () {
            int num = 5;
            int recordId = 4;
            int reId = 0;
            int nonExistingAuthorId = 50;
            RealestateDO realestate = generateRealestates(1).get(0);
            TeamProfileDO team = generateTeams(1,realestate).get(0);
            List<RealestateHistoryRecordDO> histRecords = generateReHistRecords(num, realestate, LOGGED_USER, team, RealestateChangeType.BURN, RealestateChangeType.CLAIM);

            ReActionDto reActionDto = generateReActionDto(reId, RealestateChangeType.CLAIM, nonExistingAuthorId, team.getId());

            when(userRepo.findById(LOGGED_USER.getId())).thenReturn(Optional.empty());
            when(teamProfRepo.findById(team.getId())).thenReturn(Optional.of(team));

            assertThrows(RuntimeException.class, () -> subject.createRecord(reActionDto, LOGGED_USER_DTO));
        }

        @Test
        void notExistingRealestate_throwExc () {
            int num = 5;
            int recordId = 4;
            int reId = 0;
            int nonExistingRealestateId = 50;
            RealestateDO realestate = generateRealestates(1).get(0);
            ProfileDO profile = generateProfiles(1,realestate).get(0);
            TeamProfileDO team = generateTeams(1,realestate).get(0);
            List<RealestateHistoryRecordDO> histRecords = generateReHistRecords(num, realestate, profile, team, RealestateChangeType.BURN, RealestateChangeType.CLAIM);

            ReActionDto reActionDto = generateReActionDto(nonExistingRealestateId, RealestateChangeType.CLAIM, profile.getId(), team.getId());

            when(reRepository.findById(nonExistingRealestateId)).thenReturn(Optional.empty());
            when(userRepo.findById(LOGGED_USER.getId())).thenReturn(Optional.of(LOGGED_USER));
            when(teamProfRepo.findById(team.getId())).thenReturn(Optional.of(team));

            assertThrows(RuntimeException.class, () -> subject.createRecord(reActionDto, LOGGED_USER_DTO));
        }

        @Test
        void emptyHistoryBurnAction_throwExc () {
            int reId = 0;
            RealestateDO realestate = generateRealestates(1).get(0);
            TeamProfileDO team = generateTeams(1,realestate).get(0);

            ReActionDto reActionDto = generateReActionDto(reId, RealestateChangeType.BURN, LOGGED_USER.getId(), team.getId());

            when(userRepo.findById(LOGGED_USER.getId())).thenReturn(Optional.of(LOGGED_USER));
            when(reHistoryRepo.findLastRecordForRe(reId)).thenReturn(Optional.empty());
            when(reRepository.findById(reId)).thenReturn(Optional.of(realestate));
            when(teamProfRepo.findById(team.getId())).thenReturn(Optional.of(team));

            assertThrows(RuntimeException.class, () -> subject.createRecord(reActionDto, LOGGED_USER_DTO));
        }

        @Test
        void emptyHistoryClaimAction_ok () {
            Integer expected = 5;
            int reId = 0;
            RealestateDO realestate = generateRealestates(1).get(0);
            TeamProfileDO team = generateTeams(1,realestate).get(0);

            ReActionDto reActionDto = generateReActionDto(reId, RealestateChangeType.CLAIM, LOGGED_USER.getId(), team.getId());

            RealestateHistoryRecordDO resultHist = new RealestateHistoryRecordDO();
            LogRecordDO resultLog = new LogRecordDO();

            when(reHistoryRepo.findLastRecordForRe(reId)).thenReturn(Optional.empty());
            when(reRepository.findById(reId)).thenReturn(Optional.of(realestate));
            when(userRepo.findById(LOGGED_USER.getId())).thenReturn(Optional.of(LOGGED_USER));
            when(teamProfRepo.findById(team.getId())).thenReturn(Optional.of(team));
            doAnswer(invocation -> {
                resultHist.setProfitPerMinute(expected);
                return null;
            }).when(reHistoryRepo).create(any(RealestateHistoryRecordDO.class));
            doAnswer(invocation -> {
                resultLog.setRealEstateNum(expected);
                return null;
            }).when(logRepo).addLogRecord(any(LogRecordDO.class));

            subject.createRecord(reActionDto, LOGGED_USER_DTO);

            assertEquals(expected,resultHist.getProfitPerMinute());
            assertEquals(expected,new Integer(resultLog.getRealEstateNum()));
        }

        @Test
        void emptyHistoryChangeAction_ok () {
            Integer expected = 5;
            int reId = 0;
            RealestateDO realestate = generateRealestates(1).get(0);
            TeamProfileDO team = generateTeams(1,realestate).get(0);

            ReActionDto reActionDto = generateReActionDto(reId, RealestateChangeType.CHANGE_PROFIT, LOGGED_USER.getId(), team.getId());

            RealestateHistoryRecordDO resultHist = new RealestateHistoryRecordDO();
            LogRecordDO resultLog = new LogRecordDO();

            when(reHistoryRepo.findLastRecordForRe(reId)).thenReturn(Optional.empty());
            when(reRepository.findById(reId)).thenReturn(Optional.of(realestate));
            when(userRepo.findById(LOGGED_USER.getId())).thenReturn(Optional.of(LOGGED_USER));
            when(teamProfRepo.findById(team.getId())).thenReturn(Optional.of(team));
            doAnswer(invocation -> {
                resultHist.setProfitPerMinute(expected);
                return null;
            }).when(reHistoryRepo).create(any(RealestateHistoryRecordDO.class));

            subject.createRecord(reActionDto, LOGGED_USER_DTO);

            assertEquals(expected,resultHist.getProfitPerMinute());
            assertNotEquals(expected,resultLog.getRealEstateNum());
        }

        @Test
        void notEmptyHistoryChangeAction_ok () {
            Integer expected = 5;
            int num = 5;
            int reId = 0;
            int recordId = 4;
            RealestateDO realestate = generateRealestates(1).get(0);
            TeamProfileDO team = generateTeams(1,realestate).get(0);
            List<RealestateHistoryRecordDO> histRecords = generateReHistRecords(num, realestate, LOGGED_USER, team, RealestateChangeType.BURN, RealestateChangeType.CLAIM);

            ReActionDto reActionDto = generateReActionDto(reId, RealestateChangeType.CHANGE_PROFIT, LOGGED_USER.getId(), team.getId());

            RealestateHistoryRecordDO resultHist = new RealestateHistoryRecordDO();
            LogRecordDO resultLog = new LogRecordDO();

            when(reHistoryRepo.findLastRecordForRe(reId)).thenReturn(Optional.of(histRecords.get(recordId)));
            when(reRepository.findById(reId)).thenReturn(Optional.of(realestate));
            when(userRepo.findById(LOGGED_USER.getId())).thenReturn(Optional.of(LOGGED_USER));
            when(teamProfRepo.findById(team.getId())).thenReturn(Optional.of(team));
            doAnswer(invocation -> {
                resultHist.setProfitPerMinute(expected);
                return null;
            }).when(reHistoryRepo).create(any(RealestateHistoryRecordDO.class));
            when(reHistoryRepo.findLastNonChangeProfitRecord(realestate.getId())).thenReturn(findLastNonChangeProfitRecord(histRecords));

            subject.createRecord(reActionDto, LOGGED_USER_DTO);

            assertEquals(expected, resultHist.getProfitPerMinute());
            assertNotEquals(expected,resultLog.getRealEstateNum());
        }

        @Test
        void notEmptyHistoryClaimActionAfterBurnAction_ok () {
            Integer expected = 5;
            int num = 5;
            int reId = 0;
            int recordId = 4;
            RealestateDO realestate = generateRealestates(1).get(0);
            TeamProfileDO team = generateTeams(1,realestate).get(0);
            List<RealestateHistoryRecordDO> histRecords = generateReHistRecords(num, realestate, LOGGED_USER, team, RealestateChangeType.BURN, RealestateChangeType.CLAIM);

            ReActionDto reActionDto = generateReActionDto(reId, RealestateChangeType.CLAIM, LOGGED_USER.getId(), team.getId());

            RealestateHistoryRecordDO resultHist = new RealestateHistoryRecordDO();
            LogRecordDO resultLog = new LogRecordDO();

            when(reHistoryRepo.findLastRecordForRe(reId)).thenReturn(Optional.of(histRecords.get(recordId)));
            when(reRepository.findById(reId)).thenReturn(Optional.of(realestate));
            when(userRepo.findById(LOGGED_USER.getId())).thenReturn(Optional.of(LOGGED_USER));
            when(teamProfRepo.findById(team.getId())).thenReturn(Optional.of(team));
            doAnswer(invocation -> {
                resultHist.setProfitPerMinute(expected);
                return null;
            }).when(reHistoryRepo).create(any(RealestateHistoryRecordDO.class));
            doAnswer(invocation -> {
                resultLog.setRealEstateNum(expected);
                return null;
            }).when(logRepo).addLogRecord(any(LogRecordDO.class));
            when(reHistoryRepo.findLastNonChangeProfitRecord(realestate.getId())).thenReturn(findLastNonChangeProfitRecord(histRecords));

            subject.createRecord(reActionDto, LOGGED_USER_DTO);

            assertEquals(expected,resultHist.getProfitPerMinute());
            assertEquals(expected,new Integer(resultLog.getRealEstateNum()));
        }

        @Test
        void notEmptyHistoryClaimActionAfterClaimAction_throwExc () {
            int num = 5;
            int reId = 0;
            int recordId = 4;
            RealestateDO realestate = generateRealestates(1).get(0);
            TeamProfileDO team = generateTeams(1,realestate).get(0);
            List<RealestateHistoryRecordDO> histRecords = generateReHistRecords(num, realestate, LOGGED_USER, team, RealestateChangeType.CLAIM, RealestateChangeType.CLAIM);

            ReActionDto reActionDto = generateReActionDto(reId, RealestateChangeType.CLAIM, LOGGED_USER.getId(), team.getId());

            when(reHistoryRepo.findLastRecordForRe(reId)).thenReturn(Optional.of(histRecords.get(recordId)));
            when(reRepository.findById(reId)).thenReturn(Optional.of(realestate));
            when(userRepo.findById(LOGGED_USER.getId())).thenReturn(Optional.of(LOGGED_USER));
            when(teamProfRepo.findById(team.getId())).thenReturn(Optional.of(team));
            when(reHistoryRepo.findLastNonChangeProfitRecord(realestate.getId())).thenReturn(findLastNonChangeProfitRecord(histRecords));

            assertThrows(MessageException.class, () -> subject.createRecord(reActionDto, LOGGED_USER_DTO));
        }

        @Test
        void notEmptyHistoryOnlyChangeActionClaimAction_ok () {
            Integer expected = 5;
            int num = 1;
            int reId = 0;
            int recordId = 0;
            RealestateDO realestate = generateRealestates(1).get(0);
            TeamProfileDO team = generateTeams(1,realestate).get(0);
            List<RealestateHistoryRecordDO> histRecords = generateReHistRecords(num, realestate, LOGGED_USER, team, RealestateChangeType.CHANGE_PROFIT, RealestateChangeType.CHANGE_PROFIT);

            ReActionDto reActionDto = generateReActionDto(reId, RealestateChangeType.CLAIM, LOGGED_USER.getId(), team.getId());

            RealestateHistoryRecordDO resultHist = new RealestateHistoryRecordDO();
            LogRecordDO resultLog = new LogRecordDO();

            when(reHistoryRepo.findLastRecordForRe(reId)).thenReturn(Optional.of(histRecords.get(recordId)));
            when(reRepository.findById(reId)).thenReturn(Optional.of(realestate));
            when(userRepo.findById(LOGGED_USER.getId())).thenReturn(Optional.of(LOGGED_USER));
            when(teamProfRepo.findById(team.getId())).thenReturn(Optional.of(team));
            when(reHistoryRepo.findLastNonChangeProfitRecord(realestate.getId())).thenReturn(findLastNonChangeProfitRecord(histRecords));

            doAnswer(invocation -> {
                resultHist.setProfitPerMinute(expected);
                return null;
            }).when(reHistoryRepo).create(any(RealestateHistoryRecordDO.class));
            doAnswer(invocation -> {
                resultLog.setRealEstateNum(expected);
                return null;
            }).when(logRepo).addLogRecord(any(LogRecordDO.class));

            subject.createRecord(reActionDto, LOGGED_USER_DTO);

            assertEquals(expected,resultHist.getProfitPerMinute());
            assertEquals(expected,new Integer(resultLog.getRealEstateNum()));
        }

        @Test
        void notEmptyHistoryOnlyChangeActionBurnAction_throwExc () {
            int num = 1;
            int reId = 0;
            int recordId = 0;
            RealestateDO realestate = generateRealestates(1).get(0);
            TeamProfileDO team = generateTeams(1,realestate).get(0);
            List<RealestateHistoryRecordDO> histRecords = generateReHistRecords(num, realestate, LOGGED_USER, team, RealestateChangeType.CHANGE_PROFIT, RealestateChangeType.CHANGE_PROFIT);

            ReActionDto reActionDto = generateReActionDto(reId, RealestateChangeType.BURN, LOGGED_USER.getId(), team.getId());

            when(reHistoryRepo.findLastRecordForRe(reId)).thenReturn(Optional.of(histRecords.get(recordId)));
            when(reRepository.findById(reId)).thenReturn(Optional.of(realestate));
            when(userRepo.findById(LOGGED_USER.getId())).thenReturn(Optional.of(LOGGED_USER));
            when(teamProfRepo.findById(team.getId())).thenReturn(Optional.of(team));
            when(reHistoryRepo.findLastNonChangeProfitRecord(realestate.getId())).thenReturn(findLastNonChangeProfitRecord(histRecords));

            assertThrows(MessageException.class, () -> subject.createRecord(reActionDto, LOGGED_USER_DTO));
        }
    }

    private Optional<RealestateHistoryRecordDO> findLastNonChangeProfitRecord(List<RealestateHistoryRecordDO> records) {
        return records
                .stream()
                .sorted(Comparator.comparing(RealestateHistoryRecordDO::getTimeFrom))
                .filter(record -> record.getRealestateChangeType() != RealestateChangeType.CHANGE_PROFIT)
                .reduce((fst, snd) -> snd);
    }

    private ReActionDto generateReActionDto(int reId, RealestateChangeType action, int authorId, int ownerId){
        return ReActionDto.builder()
                    .action(action)
                    .realestateId(reId)
                    .authorId(authorId)
                    .teamId(ownerId)
                .build();
    }

    private List<RealestateHistoryRecordDO> generateReHistRecords(int num, RealestateDO re, UserDO author, TeamProfileDO owner, RealestateChangeType lastRecAction, RealestateChangeType firstRecAction){
        List<RealestateHistoryRecordDO> reHistList = new ArrayList<>();
        int previousI = 0;
        for (int i=0; i < num-1; i++){
            RealestateHistoryRecordDO newRecord = RealestateHistoryRecordDO.builder()
                    .author(author)
                    .owner(owner)
                    .realestate(re)
                    .timeFrom(LocalDateTime.of(2018,8,15,previousI,0,0))
                    .timeTo(LocalDateTime.of(2018,8,15,i+1,0,0))
                    .build();

            previousI = i;

            if(i == 0){
                newRecord.setRealestateChangeType(firstRecAction);
                newRecord.setProfitPerMinute(re.getProfitPerMinute());
            } else if ((i%5)==0){
                newRecord.setRealestateChangeType(RealestateChangeType.CHANGE_PROFIT);
                re.setProfitPerMinute(re.getProfitPerMinute()+5);
            } else {
                if(reHistList.get(i-1).getRealestateChangeType().equals(RealestateChangeType.CLAIM)){
                    newRecord.setRealestateChangeType(RealestateChangeType.BURN);
                    re.setProfitPerMinute(re.getProfitPerMinute());
                } else if (reHistList.get(i-1).getRealestateChangeType().equals(RealestateChangeType.BURN)) {
                    newRecord.setRealestateChangeType(RealestateChangeType.CLAIM);
                    re.setProfitPerMinute(re.getProfitPerMinute());
                } else if (i==1) {
                    newRecord.setRealestateChangeType(RealestateChangeType.CLAIM);
                    re.setProfitPerMinute(re.getProfitPerMinute());
                } else if (reHistList.get(i-2).getRealestateChangeType().equals(RealestateChangeType.CLAIM)) {
                    newRecord.setRealestateChangeType(RealestateChangeType.BURN);
                    re.setProfitPerMinute(re.getProfitPerMinute());
                } else if (reHistList.get(i-2).getRealestateChangeType().equals(RealestateChangeType.BURN)) {
                    newRecord.setRealestateChangeType(RealestateChangeType.CLAIM);
                    re.setProfitPerMinute(re.getProfitPerMinute());
                }
            }
            newRecord.setId(i);
            reHistList.add(newRecord);
        }

        RealestateHistoryRecordDO lastRecord = RealestateHistoryRecordDO.builder()
                .author(author)
                .owner(owner)
                .realestate(re)
                .realestateChangeType(lastRecAction)
                .profitPerMinute(re.getProfitPerMinute())
                .timeFrom(LocalDateTime.of(2018,8,15,previousI,0,0))
                .build();
        lastRecord.setId(num-1);

        reHistList.add(lastRecord);

        return reHistList;
    }

    private List<RealestateDO> generateRealestates (int num) {
        List<RealestateDO> realestates = new ArrayList<>();
        for(int i=0; i<num; i++){
            RealestateDO realestate = RealestateDO.builder()
                        .districtNumber(i)
                        .name("RealEstateName@" + String.valueOf(i))
                        .price(i)
                        .profitPerMinute(i)
                        .shape("RealEstateShape@" + String.valueOf(i))
                    .build();
            realestate.setId(i);
            realestates.add(realestate);
        }
        return realestates;
    }

    private List<ProfileDO> generateProfiles(int num, RealestateDO re){
        return IntStream.range(0, num).mapToObj(i ->
                ProfileDO.profileBuilder()
                        .description("ProfileDescription@" + String.valueOf(i))
                        .gameName("ProfileGameName@" + String.valueOf(i))
                        .realestate(re)
                        .id(i)
                        .build())
                .collect(Collectors.toList());
    }

    private List<TeamProfileDO> generateTeams(int num, RealestateDO re){
        return IntStream.range(0, num).mapToObj(i ->
                TeamProfileDO.teamProfileBuilder()
                            .description("ProfileDescription@" + String.valueOf(i))
                            .realestate(re)
                            .id(i)
                            .color("TeamColor@" + String.valueOf(i))
                            .name("TeamName@" + String.valueOf(i))
                            .residenceAddress("ResidenceAddress@" + String.valueOf(i))
                            .moneyAmount(i)
                        .build())
                .collect(Collectors.toList());
    }

}
package cz.ki.mafie.service;

import com.google.common.collect.Lists;
import cz.ki.mafie.BaseTestRunner;
import cz.ki.mafie.domain.ProfileDO;
import cz.ki.mafie.domain.RealestateDO;
import cz.ki.mafie.dto.profile.ProfileForReDto;
import cz.ki.mafie.repository.profile.ProfileRepository;
import cz.ki.mafie.service.converter.ProfileConverter;
import cz.ki.mafie.service.impl.ProfileServiceImpl;
import cz.ki.mafie.utils.CategoryTag;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@Tag(CategoryTag.FAST_TEST)
class ProfileServiceTest extends BaseTestRunner {

    @InjectMocks
    private ProfileServiceImpl subject;

    @Mock
    private ProfileRepository profileRepository;

    @Nested
    class getProfilesInRe_tests extends BaseTestRunner{
        @Test
        void allConvert_ok(){
            int reId = 457;
            List<ProfileDO> profiles = generateProfiles(10, null);

            when(profileRepository.findAllByRealestate_Id(reId)).thenReturn(profiles);

            // here is using converter for conversion, not manual conversion
            // it is okay because we assuming that ProfileConverter is valid and tested
            // it is not our goal to test in each method converter, here is service logic test
            // in our case is only logic and this is just converting :-(
            List<ProfileForReDto> expected = profiles.stream().map(ProfileConverter::toDto).collect(Collectors.toList());

            List<ProfileForReDto> result = subject.getProfilesInRe(reId);

            assertEquals(expected.size(), result.size());

            expected.sort(Comparator.comparingInt(ProfileForReDto::getId));
            result.sort(Comparator.comparingInt(ProfileForReDto::getId));
            assertArrayEquals(expected.toArray(), result.toArray());
        }

        @Test
        void emptyListConvert_ok(){
            int reId = 457;
            when(profileRepository.findAllByRealestate_Id(reId)).thenReturn(Lists.newArrayList());

            List<ProfileForReDto> result = subject.getProfilesInRe(reId);

            assertEquals(0, result.size());
        }
    }

    private List<ProfileDO> generateProfiles(int num, RealestateDO re){
        return IntStream.range(0, num).mapToObj(i ->
                ProfileDO.profileBuilder()
                        .description("ProfileDescription@" + String.valueOf(i))
                        .gameName("ProfileGameName@" + String.valueOf(i))
                        .realestate(re)
                        .id(i)
                        .build())
                .collect(Collectors.toList());
    }
}
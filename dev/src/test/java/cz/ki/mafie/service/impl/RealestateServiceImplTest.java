package cz.ki.mafie.service.impl;

import cz.ki.mafie.BaseTestRunner;
import cz.ki.mafie.domain.ProfileDO;
import cz.ki.mafie.domain.RealestateDO;
import cz.ki.mafie.domain.RealestateHistoryRecordDO;
import cz.ki.mafie.domain.TeamProfileDO;
import cz.ki.mafie.domain.enumeration.RealestateChangeType;
import cz.ki.mafie.dto.realestate.RealestateDto;
import cz.ki.mafie.dto.realestateHistoryRecord.ReHistoryRecordDto;
import cz.ki.mafie.exception.MessageException;
import cz.ki.mafie.repository.log.LogRecordRepository;
import cz.ki.mafie.repository.profile.ProfileRepository;
import cz.ki.mafie.repository.realestate.RealestateRepository;
import cz.ki.mafie.repository.realestateHistoryRecord.RealestateHistoryRecordRepository;
import cz.ki.mafie.repository.teamProfile.TeamProfileRepository;
import cz.ki.mafie.utils.CategoryTag;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDateTime;
import java.util.Optional;

import static cz.ki.mafie.utils.Utils.checkNotNull;
import static cz.ki.mafie.utils.Utils.checkNullOrEmpty;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@Tag(CategoryTag.FAST_TEST)
class RealestateServiceImplTest extends BaseTestRunner{

    @InjectMocks
    private RealestateServiceImpl subject;

    @Mock
    private RealestateRepository reRepository;

    @Mock
    private LogRecordRepository logRepo;

    @Mock
    private RealestateHistoryRecordRepository reHistoryRepo;

    @Mock
    private ProfileRepository profRepo;

    @Mock
    private TeamProfileRepository teamProfRepo;

    @Nested
    class updateAndValidateRepositoryReRecord_tests extends BaseTestRunner {

        TeamProfileDO owner1;
        TeamProfileDO owner2;

        ProfileDO author1;
        ProfileDO author2;

        RealestateDO re1;
        RealestateDO re2;

        RealestateHistoryRecordDO Do;
        ReHistoryRecordDto dto;

        @BeforeEach
        void init(){
            owner1 = createOwner(1);
            owner2 = createOwner(2);

            author1 = createAuthor(1);
            author2 = createAuthor(2);

            re1 = createRealestate(400,1,1);
            re2 = createRealestate(500,2,2);

            Do = createRecordDO(owner1,re1,author1);
            dto = createRecordDTO(owner2.getId(),author2.getId(),re2.getId());
        }

        @Test
        void AllParameters_ok() {
            when(reRepository.findById(2)).thenReturn(Optional.of(re2));
            when(profRepo.findById(2)).thenReturn(Optional.of(author2));
            when(teamProfRepo.findById(2)).thenReturn(Optional.of(owner2));

            doAnswer(invocationOnMock -> null).when(reHistoryRepo).validate(any(RealestateHistoryRecordDO.class));

            RealestateHistoryRecordDO result = subject.updateAndValidateRepositoryReRecord(dto,Do);

            assertEquals(Do,result);
            assertEquals(dto.getActionType(),result.getRealestateChangeType());
            assertEquals(re2, result.getRealestate());
            assertEquals(author2, result.getAuthor());
            assertEquals(owner2, result.getOwner());
            assertEquals(dto.getTimeFrom(), result.getTimeFrom());
            assertEquals(dto.getTimeTo(), result.getTimeTo());
            assertEquals(dto.getProfitPerMinute(),result.getProfitPerMinute());
        }

        @Test
        void emptyAuthor_throwME() {
            when(profRepo.findById(2)).thenReturn(Optional.empty());

            assertThrows(MessageException.class,() ->  subject.updateAndValidateRepositoryReRecord(dto,Do));
        }

        @Test
        void emptyOwner_throwME() {
            when(profRepo.findById(2)).thenReturn(Optional.of(author2));
            when(teamProfRepo.findById(2)).thenReturn(Optional.empty());

            assertThrows(MessageException.class,() ->  subject.updateAndValidateRepositoryReRecord(dto,Do));
        }

        @Test
        void emptyRealestate_throwME() {
            when(profRepo.findById(2)).thenReturn(Optional.of(author2));
            when(teamProfRepo.findById(2)).thenReturn(Optional.of(owner2));
            when(reRepository.findById(2)).thenReturn(Optional.empty());

            assertThrows(MessageException.class,() ->  subject.updateAndValidateRepositoryReRecord(dto,Do));
        }
    }


    @Nested
    class updateAndValidateRealestate_tests extends BaseTestRunner {
        RealestateDO Do;
        RealestateDto Dto;

        @BeforeEach
        void init(){
            Do = getFilledReDO();
            Dto = getFilledReDto();
        }

        @Test
        void allParameters_ok() {
            doAnswer(invocationOnMock -> null).when(reRepository).validate(any(RealestateDO.class));

            RealestateDO result = subject.updateAndValidateRealestate(Dto,Do);

            assertEquals(Do,result);
            assertEquals(Dto.getName(), result.getName());
            assertEquals(Dto.getDistrictNumber(), result.getDistrictNumber());
            assertEquals(Dto.getProfitPerMinute(), result.getProfitPerMinute());
            assertEquals(Dto.getShape(), result.getShape());
            assertEquals(Dto.getPrice(), result.getPrice());
        }
    }

    private TeamProfileDO createOwner(int id){
        return TeamProfileDO.teamProfileBuilder()
                .id(id)
                .build();
    }

    private RealestateDO createRealestate(int profitPerMinute,int distNum,int id) {
        RealestateDO tmp =  RealestateDO.builder()
                .profitPerMinute(profitPerMinute)
                .districtNumber(distNum)
                .price(422)
                .build();
        tmp.setId(id);
        return tmp;

    }

    private ProfileDO createAuthor(int id){
        return ProfileDO.profileBuilder()
                .id(id)
                .build();
    }

    private RealestateHistoryRecordDO createRecordDO(TeamProfileDO owner, RealestateDO re, ProfileDO author){
        return RealestateHistoryRecordDO.builder()
                .author(author)
                .owner(owner)
                .realestate(re)
                .timeFrom(LocalDateTime.now())
                .timeTo(LocalDateTime.now())
                .profitPerMinute(400)
                .realestateChangeType(RealestateChangeType.BURN)
                .build();
    }

    private ReHistoryRecordDto createRecordDTO(int owner, int author, int re){
        return ReHistoryRecordDto.builder()
                .authorId(author)
                .ownerId(owner)
                .realestateId(re)
                .profitPerMinute(500)
                .timeFrom(LocalDateTime.now())
                .timeTo(LocalDateTime.now())
                .actionType(RealestateChangeType.CLAIM)
                .build();
    }

    private RealestateDO getFilledReDO(){
        return RealestateDO.builder()
                .name("test")
                .districtNumber(1)
                .price(400)
                .profitPerMinute(400)
                .shape("test")
                .build();
    }

    private RealestateDto getFilledReDto(){
        return RealestateDto.builder()
                .name("test2")
                .districtNumber(2)
                .price(500)
                .profitPerMinute(500)
                .shape("test2")
                .id(1)
                .build();
    }

}
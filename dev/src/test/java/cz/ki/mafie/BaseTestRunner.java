package cz.ki.mafie;


import cz.ki.mafie.config.AppConfig;
import cz.ki.mafie.config.PersistenceConfig;
import cz.ki.mafie.config.ServiceConfig;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.Transactional;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {PersistenceConfig.class, ServiceConfig.class})
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
@Transactional(transactionManager = "transactionManager")
@ExtendWith(MockitoExtension.class)
//@EnableAspectJAutoProxy(proxyTargetClass = true)
@EnableTransactionManagement
public abstract class BaseTestRunner {
}

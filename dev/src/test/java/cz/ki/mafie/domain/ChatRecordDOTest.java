package cz.ki.mafie.domain;


import cz.ki.mafie.BaseTestRunner;
import cz.ki.mafie.utils.CategoryTag;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import static org.junit.jupiter.api.Assertions.assertEquals;

@Tag(CategoryTag.DATABASE_TEST)
public class ChatRecordDOTest extends BaseTestRunner {

    @PersistenceContext
    private EntityManager em;

    @Test
    public void persistingCharRecord(){
        ChatRecordDO expected = new ChatRecordDO();
        em.persist(expected);
        ChatRecordDO result = em.find(ChatRecordDO.class, expected.getId());
        assertEquals(expected, result);
    }
}
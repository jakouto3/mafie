package cz.ki.mafie.domain.container;

import cz.ki.mafie.BaseTestRunner;
import cz.ki.mafie.domain.utils.JOConverter;
import cz.ki.mafie.utils.CategoryTag;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;

@Tag(CategoryTag.DATABASE_TEST)
class ContainerDOTest extends BaseTestRunner {

    @PersistenceContext
    private EntityManager em;

    @Test
    void persistingContainer () {
        ContainerDO expected = new ContainerDO();
        em.persist(expected);
        ContainerDO result = em.find(ContainerDO.class, expected.getId());
        assertEquals(expected, result);
    }

    @Test
    void defaultValuePersist () {
        ContainerDO cDO = new ContainerDO();
        em.persist(cDO);
        ContainerDO result = em.find(ContainerDO.class, cDO.getId());
        JOConverter cjc = new JOConverter();
        ContainerJO cJO;
        cJO = cjc.JSONToObject(result.getObject());

        assertNull(cJO.getGameJO().getTimeEnded());
        assertNull(cJO.getGameJO().getTimeStarted());
        assertNull(cJO.getMapJO().getImage());
        assertFalse(cJO.getMapJO().isLogVisible());
        assertNull(cJO.getGameRuleJO().getText());
    }
}
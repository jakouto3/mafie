package cz.ki.mafie.domain.utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import cz.ki.mafie.BaseTestRunner;
import cz.ki.mafie.domain.container.ContainerJO;
import cz.ki.mafie.domain.container.GameJO;
import cz.ki.mafie.domain.container.GameRuleJO;
import cz.ki.mafie.domain.container.MapJO;
import cz.ki.mafie.domain.enumeration.GameState;
import cz.ki.mafie.utils.CategoryTag;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;

@Tag(CategoryTag.FAST_TEST)
class JOConverterTest extends BaseTestRunner {
    private JOConverter subject;
    private ObjectMapper om;

    @BeforeEach
    void initialize() {
        subject = new JOConverter();
        om = new ObjectMapper();
    }

    @Test
    void objectToJSON() throws IOException {
        GameJO gJO = new GameJO();
        gJO.setGameState(GameState.STARTED);
        GameRuleJO grJO = new GameRuleJO("TestText");
        MapJO mJO = new MapJO("TestImage", true);

        ContainerJO expected = new ContainerJO(gJO,grJO,mJO);

        String JSONString = subject.objectToJSON(expected);

        ContainerJO result = om.readValue(JSONString, ContainerJO.class);

        assertEquals(expected.getGameJO().getTimeEnded(), result.getGameJO().getTimeEnded());
        assertEquals(expected.getGameJO().getTimeStarted(), result.getGameJO().getTimeStarted());
        assertEquals(expected.getGameJO().getGameState(),result.getGameJO().getGameState());
        assertEquals(expected.getMapJO().getImage(), result.getMapJO().getImage());
        assertEquals(expected.getMapJO().isLogVisible(), result.getMapJO().isLogVisible());
        assertEquals(expected.getGameRuleJO().getText(), result.getGameRuleJO().getText());
    }

    @Test
    void JSONToObject() throws IOException {
        GameJO gJO = new GameJO();
        gJO.setGameState(GameState.STARTED);
        GameRuleJO grJO = new GameRuleJO("TestText");
        MapJO mJO = new MapJO("TestImage", true);
        ContainerJO expected = new ContainerJO(gJO,grJO,mJO);

        String expectedJSON = om.writeValueAsString(expected);

        ContainerJO result = subject.JSONToObject(expectedJSON);

        assertEquals(expected.getGameJO().getTimeEnded(), result.getGameJO().getTimeEnded());
        assertEquals(expected.getGameJO().getTimeStarted(), result.getGameJO().getTimeStarted());
        assertEquals(expected.getGameJO().getGameState(), result.getGameJO().getGameState());
        assertEquals(expected.getMapJO().getImage(), result.getMapJO().getImage());
        assertEquals(expected.getMapJO().isLogVisible(), result.getMapJO().isLogVisible());
        assertEquals(expected.getGameRuleJO().getText(), result.getGameRuleJO().getText());
    }

    @Test
    void getDefaultContainerJO() throws IOException {
        String resultJSON = subject.getDefaultContainerJO();

        ContainerJO result = om.readValue(resultJSON, ContainerJO.class);

        assertNull(result.getGameJO().getTimeEnded());
        assertNull(result.getGameJO().getTimeStarted());
        assertNull(result.getMapJO().getImage());
        assertFalse(result.getMapJO().isLogVisible());
        assertNull(result.getGameRuleJO().getText());
    }
}
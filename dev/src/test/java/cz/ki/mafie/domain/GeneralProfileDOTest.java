package cz.ki.mafie.domain;

import cz.ki.mafie.BaseTestRunner;
import cz.ki.mafie.utils.CategoryTag;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import static org.junit.jupiter.api.Assertions.assertEquals;

@Tag(CategoryTag.DATABASE_TEST)
public class GeneralProfileDOTest extends BaseTestRunner {

    @PersistenceContext
    private EntityManager em;

    @Test
    public void persistingTest(){
        GeneralProfileDO expected = new GeneralProfileDO();
        expected.setDescription("description");
        em.persist(expected);

        GeneralProfileDO result = em.find(GeneralProfileDO.class, expected.getId());

        assertEquals(expected, result);
    }
}
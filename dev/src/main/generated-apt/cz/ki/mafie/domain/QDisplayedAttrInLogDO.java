package cz.ki.mafie.domain;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QDisplayedAttrInLogDO is a Querydsl query type for DisplayedAttrInLogDO
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QDisplayedAttrInLogDO extends EntityPathBase<DisplayedAttrInLogDO> {

    private static final long serialVersionUID = -1967113367L;

    public static final QDisplayedAttrInLogDO displayedAttrInLogDO = new QDisplayedAttrInLogDO("displayedAttrInLogDO");

    public final QAbstractDO _super = new QAbstractDO(this);

    //inherited
    public final NumberPath<Integer> id = _super.id;

    public final EnumPath<cz.ki.mafie.domain.enumeration.LogRecordAttributes> logRecordAttr = createEnum("logRecordAttr", cz.ki.mafie.domain.enumeration.LogRecordAttributes.class);

    public final BooleanPath value = createBoolean("value");

    public QDisplayedAttrInLogDO(String variable) {
        super(DisplayedAttrInLogDO.class, forVariable(variable));
    }

    public QDisplayedAttrInLogDO(Path<? extends DisplayedAttrInLogDO> path) {
        super(path.getType(), path.getMetadata());
    }

    public QDisplayedAttrInLogDO(PathMetadata metadata) {
        super(DisplayedAttrInLogDO.class, metadata);
    }

}


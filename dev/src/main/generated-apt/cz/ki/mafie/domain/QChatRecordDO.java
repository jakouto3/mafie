package cz.ki.mafie.domain;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QChatRecordDO is a Querydsl query type for ChatRecordDO
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QChatRecordDO extends EntityPathBase<ChatRecordDO> {

    private static final long serialVersionUID = -1506188635L;

    public static final QChatRecordDO chatRecordDO = new QChatRecordDO("chatRecordDO");

    public final QAbstractDO _super = new QAbstractDO(this);

    public final EnumPath<cz.ki.mafie.domain.enumeration.ChatRoom> chatRoom = createEnum("chatRoom", cz.ki.mafie.domain.enumeration.ChatRoom.class);

    public final StringPath content = createString("content");

    public final ListPath<GeneralProfileDO, QGeneralProfileDO> generalProfiles = this.<GeneralProfileDO, QGeneralProfileDO>createList("generalProfiles", GeneralProfileDO.class, QGeneralProfileDO.class, PathInits.DIRECT2);

    //inherited
    public final NumberPath<Integer> id = _super.id;

    public final DateTimePath<java.time.LocalDateTime> timestamp = createDateTime("timestamp", java.time.LocalDateTime.class);

    public QChatRecordDO(String variable) {
        super(ChatRecordDO.class, forVariable(variable));
    }

    public QChatRecordDO(Path<? extends ChatRecordDO> path) {
        super(path.getType(), path.getMetadata());
    }

    public QChatRecordDO(PathMetadata metadata) {
        super(ChatRecordDO.class, metadata);
    }

}


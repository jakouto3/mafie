package cz.ki.mafie.domain;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QGeneralProfileDO is a Querydsl query type for GeneralProfileDO
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QGeneralProfileDO extends EntityPathBase<GeneralProfileDO> {

    private static final long serialVersionUID = 227563005L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QGeneralProfileDO generalProfileDO = new QGeneralProfileDO("generalProfileDO");

    public final QUserDO _super = new QUserDO(this);

    public final StringPath description = createString("description");

    public final StringPath gameRole = createString("gameRole");

    //inherited
    public final NumberPath<Integer> id = _super.id;

    //inherited
    public final StringPath login = _super.login;

    //inherited
    public final StringPath password = _super.password;

    public final QRealestateDO realestate;

    //inherited
    public final EnumPath<cz.ki.mafie.domain.enumeration.SystemRole> systemRole = _super.systemRole;

    public QGeneralProfileDO(String variable) {
        this(GeneralProfileDO.class, forVariable(variable), INITS);
    }

    public QGeneralProfileDO(Path<? extends GeneralProfileDO> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QGeneralProfileDO(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QGeneralProfileDO(PathMetadata metadata, PathInits inits) {
        this(GeneralProfileDO.class, metadata, inits);
    }

    public QGeneralProfileDO(Class<? extends GeneralProfileDO> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.realestate = inits.isInitialized("realestate") ? new QRealestateDO(forProperty("realestate")) : null;
    }

}


package cz.ki.mafie.domain;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QProfileDO is a Querydsl query type for ProfileDO
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QProfileDO extends EntityPathBase<ProfileDO> {

    private static final long serialVersionUID = 1470373635L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QProfileDO profileDO = new QProfileDO("profileDO");

    public final QGeneralProfileDO _super;

    //inherited
    public final StringPath description;

    public final StringPath gameName = createString("gameName");

    //inherited
    public final StringPath gameRole;

    //inherited
    public final NumberPath<Integer> id;

    //inherited
    public final StringPath login;

    //inherited
    public final StringPath password;

    public final StringPath phoneNumber = createString("phoneNumber");

    public final StringPath photoPath = createString("photoPath");

    // inherited
    public final QRealestateDO realestate;

    public final StringPath realName = createString("realName");

    //inherited
    public final EnumPath<cz.ki.mafie.domain.enumeration.SystemRole> systemRole;

    public QProfileDO(String variable) {
        this(ProfileDO.class, forVariable(variable), INITS);
    }

    public QProfileDO(Path<? extends ProfileDO> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QProfileDO(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QProfileDO(PathMetadata metadata, PathInits inits) {
        this(ProfileDO.class, metadata, inits);
    }

    public QProfileDO(Class<? extends ProfileDO> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this._super = new QGeneralProfileDO(type, metadata, inits);
        this.description = _super.description;
        this.gameRole = _super.gameRole;
        this.id = _super.id;
        this.login = _super.login;
        this.password = _super.password;
        this.realestate = _super.realestate;
        this.systemRole = _super.systemRole;
    }

}


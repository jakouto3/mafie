package cz.ki.mafie.domain;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QTaskTemplateDO is a Querydsl query type for TaskTemplateDO
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QTaskTemplateDO extends EntityPathBase<TaskTemplateDO> {

    private static final long serialVersionUID = -403207109L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QTaskTemplateDO taskTemplateDO = new QTaskTemplateDO("taskTemplateDO");

    public final QTaskDescriptionDO _super = new QTaskDescriptionDO(this);

    public final StringPath comment = createString("comment");

    //inherited
    public final StringPath description = _super.description;

    //inherited
    public final NumberPath<Integer> id = _super.id;

    public final StringPath name = createString("name");

    public final QProfileDO owner;

    public final StringPath reward = createString("reward");

    public final EnumPath<cz.ki.mafie.domain.enumeration.TaskTemplateVisibility> visibility = createEnum("visibility", cz.ki.mafie.domain.enumeration.TaskTemplateVisibility.class);

    public QTaskTemplateDO(String variable) {
        this(TaskTemplateDO.class, forVariable(variable), INITS);
    }

    public QTaskTemplateDO(Path<? extends TaskTemplateDO> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QTaskTemplateDO(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QTaskTemplateDO(PathMetadata metadata, PathInits inits) {
        this(TaskTemplateDO.class, metadata, inits);
    }

    public QTaskTemplateDO(Class<? extends TaskTemplateDO> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.owner = inits.isInitialized("owner") ? new QProfileDO(forProperty("owner"), inits.get("owner")) : null;
    }

}


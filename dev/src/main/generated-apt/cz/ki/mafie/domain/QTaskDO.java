package cz.ki.mafie.domain;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QTaskDO is a Querydsl query type for TaskDO
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QTaskDO extends EntityPathBase<TaskDO> {

    private static final long serialVersionUID = 1313476641L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QTaskDO taskDO = new QTaskDO("taskDO");

    public final QAbstractDO _super = new QAbstractDO(this);

    public final DateTimePath<java.time.LocalDateTime> assignmentTime = createDateTime("assignmentTime", java.time.LocalDateTime.class);

    public final StringPath comment = createString("comment");

    public final StringPath description = createString("description");

    //inherited
    public final NumberPath<Integer> id = _super.id;

    public final StringPath location = createString("location");

    public final QProfileDO maintainer;

    public final QProfileDO owner;

    public final StringPath reward = createString("reward");

    public final DateTimePath<java.time.LocalDateTime> startAvailibillityTime = createDateTime("startAvailibillityTime", java.time.LocalDateTime.class);

    public final EnumPath<cz.ki.mafie.domain.enumeration.TaskState> state = createEnum("state", cz.ki.mafie.domain.enumeration.TaskState.class);

    public QTaskDO(String variable) {
        this(TaskDO.class, forVariable(variable), INITS);
    }

    public QTaskDO(Path<? extends TaskDO> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QTaskDO(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QTaskDO(PathMetadata metadata, PathInits inits) {
        this(TaskDO.class, metadata, inits);
    }

    public QTaskDO(Class<? extends TaskDO> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.maintainer = inits.isInitialized("maintainer") ? new QProfileDO(forProperty("maintainer"), inits.get("maintainer")) : null;
        this.owner = inits.isInitialized("owner") ? new QProfileDO(forProperty("owner"), inits.get("owner")) : null;
    }

}


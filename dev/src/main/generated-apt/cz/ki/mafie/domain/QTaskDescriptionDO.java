package cz.ki.mafie.domain;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QTaskDescriptionDO is a Querydsl query type for TaskDescriptionDO
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QTaskDescriptionDO extends EntityPathBase<TaskDescriptionDO> {

    private static final long serialVersionUID = 1855335441L;

    public static final QTaskDescriptionDO taskDescriptionDO = new QTaskDescriptionDO("taskDescriptionDO");

    public final QAbstractDO _super = new QAbstractDO(this);

    public final StringPath description = createString("description");

    //inherited
    public final NumberPath<Integer> id = _super.id;

    public QTaskDescriptionDO(String variable) {
        super(TaskDescriptionDO.class, forVariable(variable));
    }

    public QTaskDescriptionDO(Path<? extends TaskDescriptionDO> path) {
        super(path.getType(), path.getMetadata());
    }

    public QTaskDescriptionDO(PathMetadata metadata) {
        super(TaskDescriptionDO.class, metadata);
    }

}


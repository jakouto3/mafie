package cz.ki.mafie.domain;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QRealestateHistoryRecordDO is a Querydsl query type for RealestateHistoryRecordDO
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QRealestateHistoryRecordDO extends EntityPathBase<RealestateHistoryRecordDO> {

    private static final long serialVersionUID = 1331093141L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QRealestateHistoryRecordDO realestateHistoryRecordDO = new QRealestateHistoryRecordDO("realestateHistoryRecordDO");

    public final QAbstractDO _super = new QAbstractDO(this);

    public final QUserDO author;

    //inherited
    public final NumberPath<Integer> id = _super.id;

    public final QTeamProfileDO owner;

    public final NumberPath<Integer> profitPerMinute = createNumber("profitPerMinute", Integer.class);

    public final QRealestateDO realestate;

    public final EnumPath<cz.ki.mafie.domain.enumeration.RealestateChangeType> realestateChangeType = createEnum("realestateChangeType", cz.ki.mafie.domain.enumeration.RealestateChangeType.class);

    public final DateTimePath<java.time.LocalDateTime> timeFrom = createDateTime("timeFrom", java.time.LocalDateTime.class);

    public final DateTimePath<java.time.LocalDateTime> timeTo = createDateTime("timeTo", java.time.LocalDateTime.class);

    public QRealestateHistoryRecordDO(String variable) {
        this(RealestateHistoryRecordDO.class, forVariable(variable), INITS);
    }

    public QRealestateHistoryRecordDO(Path<? extends RealestateHistoryRecordDO> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QRealestateHistoryRecordDO(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QRealestateHistoryRecordDO(PathMetadata metadata, PathInits inits) {
        this(RealestateHistoryRecordDO.class, metadata, inits);
    }

    public QRealestateHistoryRecordDO(Class<? extends RealestateHistoryRecordDO> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.author = inits.isInitialized("author") ? new QUserDO(forProperty("author")) : null;
        this.owner = inits.isInitialized("owner") ? new QTeamProfileDO(forProperty("owner"), inits.get("owner")) : null;
        this.realestate = inits.isInitialized("realestate") ? new QRealestateDO(forProperty("realestate")) : null;
    }

}


package cz.ki.mafie.domain;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QTeamProfileDO is a Querydsl query type for TeamProfileDO
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QTeamProfileDO extends EntityPathBase<TeamProfileDO> {

    private static final long serialVersionUID = -1150667354L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QTeamProfileDO teamProfileDO = new QTeamProfileDO("teamProfileDO");

    public final QGeneralProfileDO _super;

    public final StringPath color = createString("color");

    public final QProfileDO consigliere;

    //inherited
    public final StringPath description;

    //inherited
    public final StringPath gameRole;

    //inherited
    public final NumberPath<Integer> id;

    //inherited
    public final StringPath login;

    public final NumberPath<Integer> moneyAmount = createNumber("moneyAmount", Integer.class);

    public final StringPath name = createString("name");

    //inherited
    public final StringPath password;

    // inherited
    public final QRealestateDO realestate;

    public final StringPath residenceAddress = createString("residenceAddress");

    //inherited
    public final EnumPath<cz.ki.mafie.domain.enumeration.SystemRole> systemRole;

    public final ListPath<String, StringPath> teamMembersNames = this.<String, StringPath>createList("teamMembersNames", String.class, StringPath.class, PathInits.DIRECT2);

    public QTeamProfileDO(String variable) {
        this(TeamProfileDO.class, forVariable(variable), INITS);
    }

    public QTeamProfileDO(Path<? extends TeamProfileDO> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QTeamProfileDO(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QTeamProfileDO(PathMetadata metadata, PathInits inits) {
        this(TeamProfileDO.class, metadata, inits);
    }

    public QTeamProfileDO(Class<? extends TeamProfileDO> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this._super = new QGeneralProfileDO(type, metadata, inits);
        this.consigliere = inits.isInitialized("consigliere") ? new QProfileDO(forProperty("consigliere"), inits.get("consigliere")) : null;
        this.description = _super.description;
        this.gameRole = _super.gameRole;
        this.id = _super.id;
        this.login = _super.login;
        this.password = _super.password;
        this.realestate = _super.realestate;
        this.systemRole = _super.systemRole;
    }

}


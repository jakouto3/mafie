package cz.ki.mafie.domain;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QLogRecordDO is a Querydsl query type for LogRecordDO
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QLogRecordDO extends EntityPathBase<LogRecordDO> {

    private static final long serialVersionUID = 748417871L;

    public static final QLogRecordDO logRecordDO = new QLogRecordDO("logRecordDO");

    public final QAbstractDO _super = new QAbstractDO(this);

    //inherited
    public final NumberPath<Integer> id = _super.id;

    public final EnumPath<cz.ki.mafie.domain.enumeration.RealestateChangeType> realestateChangeType = createEnum("realestateChangeType", cz.ki.mafie.domain.enumeration.RealestateChangeType.class);

    public final StringPath realEstateName = createString("realEstateName");

    public final NumberPath<Integer> realEstateNum = createNumber("realEstateNum", Integer.class);

    public final DateTimePath<java.time.LocalDateTime> recordedTime = createDateTime("recordedTime", java.time.LocalDateTime.class);

    public final StringPath teamLabel = createString("teamLabel");

    public QLogRecordDO(String variable) {
        super(LogRecordDO.class, forVariable(variable));
    }

    public QLogRecordDO(Path<? extends LogRecordDO> path) {
        super(path.getType(), path.getMetadata());
    }

    public QLogRecordDO(PathMetadata metadata) {
        super(LogRecordDO.class, metadata);
    }

}


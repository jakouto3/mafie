package cz.ki.mafie.domain;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import cz.ki.mafie.domain.container.ContainerDO;


/**
 * QContainerDO is a Querydsl query type for ContainerDO
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QContainerDO extends EntityPathBase<ContainerDO> {

    private static final long serialVersionUID = 15270939L;

    public static final QContainerDO containerDO = new QContainerDO("containerDO");

    public final QAbstractDO _super = new QAbstractDO(this);

    //inherited
    public final NumberPath<Integer> id = _super.id;

    public final StringPath object = createString("object");

    public QContainerDO(String variable) {
        super(ContainerDO.class, forVariable(variable));
    }

    public QContainerDO(Path<? extends ContainerDO> path) {
        super(path.getType(), path.getMetadata());
    }

    public QContainerDO(PathMetadata metadata) {
        super(ContainerDO.class, metadata);
    }

}


package cz.ki.mafie.domain;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QRealestateDO is a Querydsl query type for RealestateDO
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QRealestateDO extends EntityPathBase<RealestateDO> {

    private static final long serialVersionUID = 1868307206L;

    public static final QRealestateDO realestateDO = new QRealestateDO("realestateDO");

    public final QAbstractDO _super = new QAbstractDO(this);

    public final NumberPath<Integer> districtNumber = createNumber("districtNumber", Integer.class);

    //inherited
    public final NumberPath<Integer> id = _super.id;

    public final StringPath name = createString("name");

    public final NumberPath<Integer> price = createNumber("price", Integer.class);

    public final NumberPath<Integer> profitPerMinute = createNumber("profitPerMinute", Integer.class);

    public final StringPath shape = createString("shape");

    public QRealestateDO(String variable) {
        super(RealestateDO.class, forVariable(variable));
    }

    public QRealestateDO(Path<? extends RealestateDO> path) {
        super(path.getType(), path.getMetadata());
    }

    public QRealestateDO(PathMetadata metadata) {
        super(RealestateDO.class, metadata);
    }

}


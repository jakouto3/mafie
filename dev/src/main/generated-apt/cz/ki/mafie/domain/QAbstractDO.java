package cz.ki.mafie.domain;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QAbstractDO is a Querydsl query type for AbstractDO
 */
@Generated("com.querydsl.codegen.SupertypeSerializer")
public class QAbstractDO extends EntityPathBase<AbstractDO> {

    private static final long serialVersionUID = -1691908354L;

    public static final QAbstractDO abstractDO = new QAbstractDO("abstractDO");

    public final NumberPath<Integer> id = createNumber("id", Integer.class);

    public QAbstractDO(String variable) {
        super(AbstractDO.class, forVariable(variable));
    }

    public QAbstractDO(Path<? extends AbstractDO> path) {
        super(path.getType(), path.getMetadata());
    }

    public QAbstractDO(PathMetadata metadata) {
        super(AbstractDO.class, metadata);
    }

}


package cz.ki.mafie.domain;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QDisplayedTaskColumnsDO is a Querydsl query type for DisplayedTaskColumnsDO
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QDisplayedTaskColumnsDO extends EntityPathBase<DisplayedTaskColumnsDO> {

    private static final long serialVersionUID = 1341502227L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QDisplayedTaskColumnsDO displayedTaskColumnsDO = new QDisplayedTaskColumnsDO("displayedTaskColumnsDO");

    public final QAbstractDO _super = new QAbstractDO(this);

    public final QGeneralProfileDO generalProfileDO;

    //inherited
    public final NumberPath<Integer> id = _super.id;

    public final EnumPath<cz.ki.mafie.domain.enumeration.TaskColumn> taskColumn = createEnum("taskColumn", cz.ki.mafie.domain.enumeration.TaskColumn.class);

    public final BooleanPath value = createBoolean("value");

    public QDisplayedTaskColumnsDO(String variable) {
        this(DisplayedTaskColumnsDO.class, forVariable(variable), INITS);
    }

    public QDisplayedTaskColumnsDO(Path<? extends DisplayedTaskColumnsDO> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QDisplayedTaskColumnsDO(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QDisplayedTaskColumnsDO(PathMetadata metadata, PathInits inits) {
        this(DisplayedTaskColumnsDO.class, metadata, inits);
    }

    public QDisplayedTaskColumnsDO(Class<? extends DisplayedTaskColumnsDO> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.generalProfileDO = inits.isInitialized("generalProfileDO") ? new QGeneralProfileDO(forProperty("generalProfileDO"), inits.get("generalProfileDO")) : null;
    }

}


package cz.ki.mafie.domain;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QNotificationDO is a Querydsl query type for NotificationDO
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QNotificationDO extends EntityPathBase<NotificationDO> {

    private static final long serialVersionUID = -371244793L;

    public static final QNotificationDO notificationDO = new QNotificationDO("notificationDO");

    public final QAbstractDO _super = new QAbstractDO(this);

    public final StringPath content = createString("content");

    //inherited
    public final NumberPath<Integer> id = _super.id;

    public final DateTimePath<java.time.LocalDateTime> recordedTime = createDateTime("recordedTime", java.time.LocalDateTime.class);

    public final StringPath typeDescription = createString("typeDescription");

    public QNotificationDO(String variable) {
        super(NotificationDO.class, forVariable(variable));
    }

    public QNotificationDO(Path<? extends NotificationDO> path) {
        super(path.getType(), path.getMetadata());
    }

    public QNotificationDO(PathMetadata metadata) {
        super(NotificationDO.class, metadata);
    }

}


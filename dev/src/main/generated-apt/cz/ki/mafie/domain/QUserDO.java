package cz.ki.mafie.domain;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QUserDO is a Querydsl query type for UserDO
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QUserDO extends EntityPathBase<UserDO> {

    private static final long serialVersionUID = 1358318823L;

    public static final QUserDO userDO = new QUserDO("userDO");

    public final QAbstractDO _super = new QAbstractDO(this);

    //inherited
    public final NumberPath<Integer> id = _super.id;

    public final StringPath login = createString("login");

    public final StringPath password = createString("password");

    public final EnumPath<cz.ki.mafie.domain.enumeration.SystemRole> systemRole = createEnum("systemRole", cz.ki.mafie.domain.enumeration.SystemRole.class);

    public QUserDO(String variable) {
        super(UserDO.class, forVariable(variable));
    }

    public QUserDO(Path<? extends UserDO> path) {
        super(path.getType(), path.getMetadata());
    }

    public QUserDO(PathMetadata metadata) {
        super(UserDO.class, metadata);
    }

}


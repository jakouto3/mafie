package cz.ki.mafie.controller;

import com.google.common.collect.Lists;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import java.io.Serializable;
import java.util.List;

@Controller
@RequestMapping("/")
public class ListController {

    @RequestMapping(value = "/users/", method = RequestMethod.GET)
    public ModelAndView getUsers() {

        List<UserDto> list = Lists.newArrayList(new UserDto("5","username","firstName","lastName"));

        ModelAndView model = new ModelAndView("list");
        model.addObject("users", list);
        return model;
    }

    @Getter  @Setter
    public class UserDto implements Serializable {

        private String id;
        private String username;
        private String firstName;
        private String lastName;

        public UserDto() {
        }

        public UserDto(String id, String username, String firstName, String lastName) {
            this.id = id;
            this.username = username;
            this.firstName = firstName;
            this.lastName = lastName;
        }
    }
}

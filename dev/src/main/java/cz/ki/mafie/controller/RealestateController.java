package cz.ki.mafie.controller;

import cz.ki.mafie.domain.enumeration.RealestateChangeType;
import cz.ki.mafie.domain.enumeration.SystemRole;
import cz.ki.mafie.dto.UserDto;
import cz.ki.mafie.dto.realestate.ReActionDto;
import cz.ki.mafie.dto.realestate.ReDetailOverviewDto;
import cz.ki.mafie.dto.realestate.RealestateDto;
import cz.ki.mafie.service.RealestateService;
import cz.ki.mafie.service.TeamProfileService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import javax.annotation.PostConstruct;

@Controller
@RequestMapping("/realestate")
@Slf4j
public class RealestateController {

    @Autowired
    private RealestateService reService;

    @Autowired
    private TeamProfileService teamProfileService;

    private UserDto currentUser;

    @PostConstruct
    public void init(){
        currentUser = UserDto.builder()
                .id(16)
                .roles(SystemRole.GAMEMASTER)
                .username("ovodicka")
                .build();
    }

    @RequestMapping(value = "/")
    public ModelAndView listRealestate(){
        ModelAndView model = new ModelAndView("realestate/list");
        model.addObject("title", "Nemovitosti | Mafie");
        model.addObject("realestates", reService.findAllByOrderByDistrictNumber());
        return model;
    }

    @RequestMapping(value = "/detail/{id}", method = {RequestMethod.GET, RequestMethod.POST})
    public ModelAndView realestateDetail(@PathVariable int id, Model md){
        ReDetailOverviewDto re;
        try{
            re = reService.getReDetail(id);
        } catch (Exception e) {
            //error 500
            log.error(e.getMessage(), e);
            return returnError500(e);
        }

        ModelAndView model = new ModelAndView("realestate/detail");
        model.addObject("title", "Detail nemovitosti | Mafie");
        model.addObject("re", re.getRealestateDto());
        model.addObject("historyRecords", re.getReHistoryRecords());
        model.addObject("teams", teamProfileService.findAll());
        model.addObject("message", (String) md.asMap().get("message"));
        model.addObject("message_type", (String) md.asMap().get("message_type"));

        return model;
    }

    @RequestMapping(value = "/detail/edit/{id}", method = RequestMethod.POST)
    public ModelAndView realestateEdit(@PathVariable int id,
                                       @RequestParam(value="profit", required=true) Integer profitPerMinute,
                                       @RequestParam(value="price", required=true) Integer price,
                                       RedirectAttributes redirAtrr){
        ReDetailOverviewDto re;
        try{
            re = reService.getReDetail(id);
        } catch (Exception e) {
            //error 500
            log.error(e.getMessage(), e);
            redirAtrr.addFlashAttribute("message", e.getMessage());
            redirAtrr.addFlashAttribute("message_type", "alert-danger");
            return new ModelAndView(new RedirectView("../"+id));
        }

        RealestateDto realestateDto = re.getRealestateDto();
        realestateDto.setProfitPerMinute(profitPerMinute);
        realestateDto.setPrice(price);
        try{
            reService.updateReInfo(realestateDto, currentUser);
        } catch (Exception e){
            log.error(e.getMessage(), e);
            redirAtrr.addFlashAttribute("message", e.getMessage());
            redirAtrr.addFlashAttribute("message_type", "alert-danger");
            return new ModelAndView(new RedirectView("../"+id));
        }
        redirAtrr.addFlashAttribute("message", "Změna byla uložena.");
        redirAtrr.addFlashAttribute("message_type", "alert-success");
        return new ModelAndView(new RedirectView("../"+id));
    }

    @RequestMapping(value = "/detail/sell/{id}", method = {RequestMethod.POST})
    public ModelAndView realestateSell(@PathVariable int id,
                                       @RequestParam(value="teamId", required=true) Integer teamId,
                                       RedirectAttributes redirAtrr){
        if(!reService.canBeClaimed(id)) {
            redirAtrr.addFlashAttribute("message", "Budova nemůže být prodána!");
            redirAtrr.addFlashAttribute("message_type", "alert-danger");
            return new ModelAndView(new RedirectView("../"+id));
        }
        try{
            reService.createRecord(new ReActionDto(id, RealestateChangeType.CLAIM, null, teamId), currentUser);
        } catch(Exception e){
            log.error(e.getMessage(), e);
            redirAtrr.addFlashAttribute("message", e.getMessage());
            redirAtrr.addFlashAttribute("message_type", "alert-danger");
            return new ModelAndView(new RedirectView("../"+id));
        }
        redirAtrr.addFlashAttribute("message", "Budova byla prodána.");
        redirAtrr.addFlashAttribute("message_type", "alert-success");
        return new ModelAndView(new RedirectView("../"+id));
    }

    @RequestMapping(value = "/detail/burn/{id}", method = {RequestMethod.POST})
    public ModelAndView realestateBurn(@PathVariable int id,
                                       @RequestParam(value="teamId", required=true) Integer teamId,
                                       RedirectAttributes redirAtrr){
        if(!reService.canBeBurned(id)) {
            redirAtrr.addFlashAttribute("message", "Budova nemůže být vypálena!");
            redirAtrr.addFlashAttribute("message_type", "alert-danger");
            return new ModelAndView(new RedirectView("../"+id));
        }
        try{
            reService.createRecord(new ReActionDto(id, RealestateChangeType.BURN, null, teamId), currentUser);
        } catch(Exception e){
            log.error(e.getMessage(), e);
            redirAtrr.addFlashAttribute("message", e.getMessage());
            redirAtrr.addFlashAttribute("message_type", "alert-danger");
            return new ModelAndView(new RedirectView("../"+id));
        }
        redirAtrr.addFlashAttribute("message", "Budova byla vypálena.");
        redirAtrr.addFlashAttribute("message_type", "alert-success");
        return new ModelAndView(new RedirectView("../"+id));
    }

    @RequestMapping(value = "/detail/{id}/deleteRecord/{reHistoryId}", method = {RequestMethod.GET})
    public ModelAndView realestateDeleteRecord(@PathVariable int id, @PathVariable int reHistoryId, RedirectAttributes redirAtrr){
        if(!reService.canDeleteRecord(reHistoryId)) {
            redirAtrr.addFlashAttribute("message", "Záznam nemůže být vymazán!");
            redirAtrr.addFlashAttribute("message_type", "alert-danger");
            return new ModelAndView(new RedirectView("../../"+id));
        }
        try{
            reService.deleteRecord(reHistoryId);
        } catch(Exception e){
            log.error(e.getMessage(), e);
            redirAtrr.addFlashAttribute("message", e.getMessage());
            redirAtrr.addFlashAttribute("message_type", "alert-danger");
            return new ModelAndView(new RedirectView("../../"+id));
        }
        redirAtrr.addFlashAttribute("message", "Záznam historie byl vymazán.");
        redirAtrr.addFlashAttribute("message_type", "alert-success");
        return new ModelAndView(new RedirectView("../../"+id));
    }

    private ModelAndView returnError500 (Exception e){
        ModelAndView model = new ModelAndView("error500");
        model.addObject("message", e.getMessage());
        return model;
    }
}

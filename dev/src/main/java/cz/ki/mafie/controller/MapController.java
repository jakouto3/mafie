package cz.ki.mafie.controller;


import com.fasterxml.jackson.databind.ObjectMapper;
import cz.ki.mafie.dto.map.FullMapDto;
import cz.ki.mafie.service.MapService;
import cz.ki.mafie.service.mock.MapServiceMock;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/map")
@Slf4j
public class MapController {
    private MapService mapService = new MapServiceMock();

    @RequestMapping(value = "/")
    public ModelAndView getMap(){
        ModelAndView model = new ModelAndView("/map/default");
        FullMapDto mapDto = mapService.getMapInfo();
        model.addObject("title", "Mapa | Mafie");
        model.addObject("teams", mapDto.getTeams());
        model.addObject("realestates", mapDto.getRealEstates());
        return model;
    }

    @RequestMapping(value = "/data/", produces = "application/json")
    public ModelAndView getData(){
        ModelAndView model = new ModelAndView("/map/dataResponse");
        String jsonResponse = null;
        ObjectMapper mapper = new ObjectMapper();
        try{
            jsonResponse = mapper.writeValueAsString(mapService.getMapInfo());
        } catch(Exception e){
            log.error(e.getMessage());
        }
        model.addObject("response", jsonResponse);
        return model;
    }
}

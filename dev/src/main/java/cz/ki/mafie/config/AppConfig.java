package cz.ki.mafie.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.annotation.Import;

@Configuration
//@EnableAspectJAutoProxy(proxyTargetClass = true)
@Import({PersistenceConfig.class, FeConfig.class, ServiceConfig.class})
public class AppConfig {
}

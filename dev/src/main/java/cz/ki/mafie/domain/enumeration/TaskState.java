package cz.ki.mafie.domain.enumeration;

public enum TaskState {
    OPEN("OPEN"), CLOSED("CLOSED");

    private final String name;

    TaskState(String name) { this.name = name;}

    @Override
    public String toString() {return name;}
}

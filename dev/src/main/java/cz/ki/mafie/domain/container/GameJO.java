package cz.ki.mafie.domain.container;

import cz.ki.mafie.domain.enumeration.GameState;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import java.time.LocalDateTime;

@Getter @Setter @Builder @AllArgsConstructor
public class GameJO{

    private LocalDateTime timeEnded;
    private LocalDateTime timeStarted;
    private GameState gameState;

    public GameJO() {

    }
}

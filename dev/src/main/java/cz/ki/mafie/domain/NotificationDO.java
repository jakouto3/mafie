package cz.ki.mafie.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "notifications")
@Getter @Setter @Builder @AllArgsConstructor
public class NotificationDO extends AbstractDO{

    @Column
    private String content;

    @Column(name = "recorder_time")
    @Temporal(TemporalType.TIMESTAMP)
    private LocalDateTime recordedTime;

    @Column(name = "type_descr")
    private String typeDescription;

    public NotificationDO() {
    }
}

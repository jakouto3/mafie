package cz.ki.mafie.domain.enumeration;

public enum LogRecordAttributes {
    REAL_ESTATE_NAME("REAL_ESTATE_NAME"),
    REAL_ESTATE_NUM("REAL_ESTATE_NUM"),
    RECORDER_TIME("RECORDER_TIME"),
    TEAM("TEAM");

    private final String name;

    LogRecordAttributes(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}

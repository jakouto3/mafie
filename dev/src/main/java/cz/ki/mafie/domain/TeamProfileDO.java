package cz.ki.mafie.domain;

import cz.ki.mafie.domain.enumeration.SystemRole;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.Singular;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "team_profiles")
@DiscriminatorValue("team")
@Getter @Setter
public class TeamProfileDO extends GeneralProfileDO{

    @Column(name = "money_amount")
    private int moneyAmount;

    @Column
    private String color;

    @Column(name = "address")
    private String residenceAddress;

    @Column
    private String name;

    @Singular
    @ElementCollection(fetch = FetchType.EAGER)
    private List<String> teamMembersNames;

    @ManyToOne
    private ProfileDO consigliere;

    public TeamProfileDO() {
    }

    @Builder(builderMethodName = "teamProfileBuilder")
    public TeamProfileDO(Integer id, String login, String password, SystemRole systemRole, String description, String gameRole, RealestateDO realestate, int moneyAmount, String color, String residenceAddress, String name, List<String> teamMembersNames, ProfileDO consigliere) {
        super(id, login, password, systemRole, description, gameRole, realestate);
        this.moneyAmount = moneyAmount;
        this.color = color;
        this.residenceAddress = residenceAddress;
        this.name = name;
        this.teamMembersNames = teamMembersNames;
        this.consigliere = consigliere;
    }
}

package cz.ki.mafie.domain.enumeration;

public enum RealestateChangeType {
    BURN("BURN"),
    CLAIM("CLAIM"),
    CHANGE_PROFIT("CHANGE_PROFIT");

    private final String name;

    RealestateChangeType(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }

    public boolean isOpposite(RealestateChangeType ct2){
        return (this.equals(BURN) && ct2.equals(CLAIM)) || (this.equals(CLAIM) && ct2.equals(BURN));
    }
}

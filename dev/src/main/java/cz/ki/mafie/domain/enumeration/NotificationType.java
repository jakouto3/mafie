package cz.ki.mafie.domain.enumeration;

public enum NotificationType {
    CHANGE_REALESTATE_COST("CHANGE_REALESTATE_COST"),
    TASK_DELETION("TASK_DELETION"),
    GAME_STATE_CHANGE("GAME_STATE_CHANGE"),
    OTHER("OTHER");

    private final String name;

    NotificationType(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}
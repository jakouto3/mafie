package cz.ki.mafie.domain;

import cz.ki.mafie.domain.enumeration.TaskTemplateVisibility;
import lombok.*;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "task_templates")
@DiscriminatorValue("templates")
@Getter @Setter
public class TaskTemplateDO extends TaskDescriptionDO{

    @Column
    private String comment;

    @Column
    private String name;

    @Column
    private String reward;

    @Enumerated(EnumType.STRING)
    private TaskTemplateVisibility visibility;

    @ManyToOne
    @JoinColumn(name = "id_owner")
    private ProfileDO owner;

    public TaskTemplateDO() {
    }

    @Builder(builderMethodName = "taskTemplateBuilder")
    public TaskTemplateDO(String description, String comment, String name, String reward, TaskTemplateVisibility visibility, ProfileDO owner) {
        super(description);
        this.comment = comment;
        this.name = name;
        this.reward = reward;
        this.visibility = visibility;
        this.owner = owner;
    }
}

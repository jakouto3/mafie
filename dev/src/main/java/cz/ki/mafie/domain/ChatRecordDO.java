package cz.ki.mafie.domain;

import cz.ki.mafie.domain.enumeration.ChatRoom;
import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

@Entity
@Table(name = "chat_records")
@Getter @Setter @Builder @AllArgsConstructor
public class ChatRecordDO extends AbstractDO{

    @Column
    private String content;

    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private LocalDateTime timestamp;

    @Enumerated(EnumType.STRING)
    private ChatRoom chatRoom;

    @Singular
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "chatrecord_generalprofile",
            joinColumns = {@JoinColumn(name = "id_gen_prof")},
            inverseJoinColumns = {@JoinColumn(name = "id_chat_record")})
    private List<GeneralProfileDO> generalProfiles;

    public ChatRecordDO() {
    }
}

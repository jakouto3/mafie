package cz.ki.mafie.domain;

public final class Q {
    private Q() {
    }

    public static final QRealestateDO Q_RE = QRealestateDO.realestateDO;
    public static final QGeneralProfileDO Q_GEN_PROFILE = QGeneralProfileDO.generalProfileDO;
    public static final QNotificationDO Q_NOTIFICATION = QNotificationDO.notificationDO;
    public static final QChatRecordDO Q_CHAT_RECORD = QChatRecordDO.chatRecordDO;
    public static final QContainerDO Q_CONTAINER = QContainerDO.containerDO;
    public static final QDisplayedAttrInLogDO Q_DISPL_ATTR_IN_LOG = QDisplayedAttrInLogDO.displayedAttrInLogDO;
    public static final QDisplayedTaskColumnsDO Q_DISPL_TASK_COLUMNS = QDisplayedTaskColumnsDO.displayedTaskColumnsDO;
    public static final QLogRecordDO Q_LOG_RECORD = QLogRecordDO.logRecordDO;
    public static final QProfileDO Q_PROFILE = QProfileDO.profileDO;
    public static final QRealestateHistoryRecordDO Q_RE_HISTORY_RECORD = QRealestateHistoryRecordDO.realestateHistoryRecordDO;
    public static final QTaskDescriptionDO Q_TASK_DESCRIPTION = QTaskDescriptionDO.taskDescriptionDO;
    public static final QTaskDO Q_TASK = QTaskDO.taskDO;
    public static final QTaskTemplateDO Q_TASK_TEMPLATE = QTaskTemplateDO.taskTemplateDO;
    public static final QTeamProfileDO Q_TEAM_PROFILE = QTeamProfileDO.teamProfileDO;
    public static final QUserDO Q_USER = QUserDO.userDO;
}

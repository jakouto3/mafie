package cz.ki.mafie.domain.enumeration;

public enum TaskTemplateVisibility {
    PUBLIC("PUBLIC"),
    PRIVATE("PRIVATE");

    private final String name;

    TaskTemplateVisibility(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}

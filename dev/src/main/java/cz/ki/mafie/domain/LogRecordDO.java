package cz.ki.mafie.domain;

import cz.ki.mafie.domain.enumeration.RealestateChangeType;
import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "log_records")
@Getter @Setter @Builder @AllArgsConstructor @ToString
public class LogRecordDO extends AbstractDO{

    @Column(name = "re_name")
    private String realEstateName;

    @Column(name = "re_num")
    private int realEstateNum;

    @Column(name = "recorded_time")
    @Temporal(TemporalType.TIMESTAMP)
    private LocalDateTime recordedTime;

    @Column(name = "team_label")
    private String teamLabel;

    @Enumerated(EnumType.STRING)
    private RealestateChangeType realestateChangeType;

    public LogRecordDO() {
    }
}

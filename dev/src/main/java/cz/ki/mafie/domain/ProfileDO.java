package cz.ki.mafie.domain;

import cz.ki.mafie.domain.enumeration.SystemRole;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "profiles")
@DiscriminatorValue(value = "profile")
@Inheritance(strategy = InheritanceType.JOINED)
@Getter @Setter
public class ProfileDO extends GeneralProfileDO{

    @Column(name = "game_name")
    private String gameName;

    @Column(name = "phone_num")
    private String phoneNumber;

    @Column(name = "photo_path")
    private String photoPath;

    @Column(name = "real_name")
    private String realName;

    public ProfileDO() {
    }

    @Builder(builderMethodName = "profileBuilder")
    public ProfileDO(Integer id, String login, String password, SystemRole systemRole, String description, String gameRole, RealestateDO realestate, String gameName, String phoneNumber, String photoPath, String realName) {
        super(id, login, password, systemRole, description, gameRole, realestate);
        this.gameName = gameName;
        this.phoneNumber = phoneNumber;
        this.photoPath = photoPath;
        this.realName = realName;
    }
}

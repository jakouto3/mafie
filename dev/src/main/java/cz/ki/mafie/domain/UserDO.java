package cz.ki.mafie.domain;

import cz.ki.mafie.domain.enumeration.SystemRole;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "users")
@DiscriminatorColumn(name = "obj_id", discriminatorType = DiscriminatorType.STRING)
@DiscriminatorValue(value = "user")
@Getter @Setter
public class UserDO extends AbstractDO{

    @Column
    private String login;

    @Column
    private String password;

    @Enumerated(EnumType.STRING)
    @Column(name = "system_role")
    private SystemRole systemRole;

    public UserDO() {
    }

    @Builder(builderMethodName = "userBuilder")
    public UserDO(Integer id, String login, String password, SystemRole systemRole) {
        super(id);
        this.login = login;
        this.password = password;
        this.systemRole = systemRole;
    }
}

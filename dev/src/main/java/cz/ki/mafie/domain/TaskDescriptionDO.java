package cz.ki.mafie.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "task_descrs")
@DiscriminatorColumn(name = "obj_id", discriminatorType = DiscriminatorType.STRING)
@DiscriminatorValue("description")
@Getter @Setter @Builder @AllArgsConstructor
public class TaskDescriptionDO extends AbstractDO{

    @Column
    private String description;

    public TaskDescriptionDO() {
    }
}

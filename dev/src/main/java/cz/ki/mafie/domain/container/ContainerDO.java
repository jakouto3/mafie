package cz.ki.mafie.domain.container;

import com.fasterxml.jackson.core.JsonProcessingException;
import cz.ki.mafie.domain.AbstractDO;
import cz.ki.mafie.domain.utils.JOConverter;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.PrePersist;
import javax.persistence.Table;

@Entity
@Table(name = "containers")
@Getter @Setter @Builder @AllArgsConstructor
public class ContainerDO extends AbstractDO {

    @Column
    private String object;

    @PrePersist
    void preInsert() {
        if(this.object == null){
            JOConverter cjc = new JOConverter();
            this.object = cjc.getDefaultContainerJO();
        }
    }

    public ContainerDO() {
    }
}

package cz.ki.mafie.domain;

import lombok.*;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import java.io.Serializable;

@EqualsAndHashCode
@MappedSuperclass
public class AbstractDO implements Serializable {

    @Getter
    @Setter
    @Id
    @GeneratedValue
    protected Integer id;

    public AbstractDO() {
    }

    @Builder(builderMethodName = "abstractBuilder")
    public AbstractDO(Integer id) {
        this.id = id;
    }
}

package cz.ki.mafie.domain.container;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter @Setter @Builder @AllArgsConstructor
public class ContainerJO {
    private GameJO gameJO;
    private GameRuleJO gameRuleJO;
    private MapJO mapJO;

    public ContainerJO() {

    }
}
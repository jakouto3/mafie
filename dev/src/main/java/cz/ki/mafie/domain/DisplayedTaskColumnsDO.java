package cz.ki.mafie.domain;

import cz.ki.mafie.domain.enumeration.TaskColumn;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "displ_task_cols")
@Getter @Setter @Builder @AllArgsConstructor
public class DisplayedTaskColumnsDO extends AbstractDO{

    @Column(name = "task_col")
    private TaskColumn taskColumn;

    @Column
    private boolean value;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "gen_profile")
    private GeneralProfileDO generalProfileDO;

    public DisplayedTaskColumnsDO() {
    }
}

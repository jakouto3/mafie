package cz.ki.mafie.domain;

import cz.ki.mafie.domain.enumeration.SystemRole;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "gen_profiles")
@DiscriminatorValue(value = "general_profile")
@Inheritance(strategy = InheritanceType.JOINED)
@Getter @Setter
public class GeneralProfileDO extends UserDO{

    @Column
    private String description;

    @Column(name = "game_role")
    private String gameRole;

    @ManyToOne
    @JoinColumn(name = "id_realestate")
    private RealestateDO realestate;

    public GeneralProfileDO() {
    }

    @Builder(builderMethodName = "generalProfileBuilder")
    public GeneralProfileDO(Integer id, String login, String password, SystemRole systemRole, String description, String gameRole, RealestateDO realestate) {
        super(id, login, password, systemRole);
        this.description = description;
        this.gameRole = gameRole;
        this.realestate = realestate;
    }
}

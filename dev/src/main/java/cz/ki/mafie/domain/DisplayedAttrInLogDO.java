package cz.ki.mafie.domain;

import cz.ki.mafie.domain.enumeration.LogRecordAttributes;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "displayed_attrs_in_log")
@Getter @Setter @Builder @AllArgsConstructor
public class DisplayedAttrInLogDO extends AbstractDO{

    @Enumerated(EnumType.STRING)
    private LogRecordAttributes logRecordAttr;

    @Column
    private boolean value;

    public DisplayedAttrInLogDO() {
    }
}

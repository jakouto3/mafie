package cz.ki.mafie.domain.enumeration;

public enum GameState {
    PREPARED("PREPARED"),
    STARTED("STARTED"),
    ENDED("ENDED");

    private final String name;

    GameState(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}

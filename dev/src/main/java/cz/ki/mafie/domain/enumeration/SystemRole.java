package cz.ki.mafie.domain.enumeration;

public enum SystemRole {
    ADMIN("ADMIN"),
    GAMEMASTER("GAMEMASTER"),
    TEAM("TEAM"),
    CP("CP"),
    KONCILIERY("KONCILIERY"),
    ASSASSIN("ASSASSIN");

    private final String name;

    SystemRole(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}

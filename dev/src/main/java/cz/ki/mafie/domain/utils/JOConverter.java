package cz.ki.mafie.domain.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import cz.ki.mafie.domain.container.ContainerJO;
import cz.ki.mafie.domain.container.GameJO;
import cz.ki.mafie.domain.container.GameRuleJO;
import cz.ki.mafie.domain.container.MapJO;
import lombok.SneakyThrows;

import java.io.IOException;

public class JOConverter {

    private static ObjectMapper om;

    public JOConverter() {
        om = new ObjectMapper();
    }

    @SneakyThrows
    public String objectToJSON (ContainerJO toConvert) {
        return om.writeValueAsString(toConvert);
    }

    @SneakyThrows
    public ContainerJO JSONToObject (String JSONString) {
        return om.readValue(JSONString, ContainerJO.class);
    }

    @SneakyThrows
    public String getDefaultContainerJO() {
        GameJO gameJO = new GameJO();
        GameRuleJO gameRuleJO = new GameRuleJO();
        MapJO mapJO = new MapJO();

        ContainerJO containerJO = new ContainerJO();
        containerJO.setGameJO(gameJO);
        containerJO.setGameRuleJO(gameRuleJO);
        containerJO.setMapJO(mapJO);

        return om.writeValueAsString(containerJO);
    }
}

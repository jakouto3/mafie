package cz.ki.mafie.domain.enumeration;

public enum NotificationState {
    NOT_DISPLAYED("NOT_DISPLAYED"),
    DISPLAYED("DISPLAYED"),
    ARCHIVED("ARCHIVED");

    private final String name;

    NotificationState(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}

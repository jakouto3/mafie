package cz.ki.mafie.domain;

import cz.ki.mafie.domain.enumeration.RealestateChangeType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "re_history_records")
@Getter @Setter @Builder @AllArgsConstructor
public class RealestateHistoryRecordDO extends AbstractDO{


    @Column(name = "profit_per_min", nullable = false)
    private Integer profitPerMinute;

    @Column(name = "time_from")
    @Temporal(TemporalType.TIMESTAMP)
    private LocalDateTime timeFrom;

    @Column(name = "time_to")
    @Temporal(TemporalType.TIMESTAMP)
    private LocalDateTime timeTo;

    @Enumerated(EnumType.STRING)
    @Column(name = "re_change_type")
    private RealestateChangeType realestateChangeType;

    @ManyToOne
    @JoinColumn(name = "id_re")
    private RealestateDO realestate;

    @ManyToOne
    @JoinColumn(name = "id_team")
    private TeamProfileDO owner;

    @ManyToOne
    @JoinColumn(name = "id_author")
    private UserDO author;

    public RealestateHistoryRecordDO() {
    }

    @Override
    public String toString() {
        return "RealestateHistoryRecordDO{" +
                "profitPerMinute=" + profitPerMinute +
                ", timeFrom=" + timeFrom +
                ", timeTo=" + timeTo +
                ", realestateChangeType=" + realestateChangeType +
                ", id=" + id +
                '}';
    }
}

package cz.ki.mafie.domain;

import cz.ki.mafie.domain.enumeration.TaskState;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "tasks")
@Getter @Setter @Builder @AllArgsConstructor
public class TaskDO extends AbstractDO{

    @Column(name = "assignment_time")
    @Temporal(TemporalType.TIMESTAMP)
    private LocalDateTime assignmentTime;

    @Column(name = "start_availible_time")
    @Temporal(TemporalType.TIMESTAMP)
    private LocalDateTime startAvailibillityTime;

    @Column
    private String comment;

    @Column
    private String description;

    @Column
    private String location;

    @Column
    private String reward;

    @Enumerated(EnumType.STRING)
    private TaskState state;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_owner")
    private ProfileDO owner;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_maintainer")
    private ProfileDO maintainer;

    public TaskDO() {
    }
}

package cz.ki.mafie.domain.enumeration;

public enum TaskColumn {
    START_AVAILIBILITY_TIME("START_AVAILIBILITY_TIME"),
    ASSIGNMENT_TIME("ASSIGNMENT_TIME"),
    DESCRIPTION("DESCRIPTION"),
    MAINTAINER("MAINTAINER"),
    COMMENT("COMMENT"),
    LOCATION("LOCATION"),
    REWARD("REWARD"),
    STATE("STATE");

    private final String name;

    TaskColumn(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}

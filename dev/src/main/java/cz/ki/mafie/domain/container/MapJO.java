package cz.ki.mafie.domain.container;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter @Setter @Builder @AllArgsConstructor
public class MapJO{

    private String image;
    private boolean isLogVisible;

    public MapJO () {

    }
}

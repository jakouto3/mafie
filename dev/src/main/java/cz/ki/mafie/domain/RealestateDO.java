package cz.ki.mafie.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "realestates")
@Getter @Setter @Builder @AllArgsConstructor
public class RealestateDO extends AbstractDO{

    @Column(name = "district_num", unique = true, nullable = false)
    private Integer districtNumber;

    @Column
    private String name;

    @Column(nullable = false)
    private Integer price;

    @Column(name = "profit_per_min", nullable = false)
    private Integer profitPerMinute;

    @Column(length = 900)
    private String shape;

    public RealestateDO() {
    }
}

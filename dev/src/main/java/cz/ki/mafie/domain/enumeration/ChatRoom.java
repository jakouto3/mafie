package cz.ki.mafie.domain.enumeration;

public enum ChatRoom {
    L1_L2_LEADERS("L1_L2_LEADERS"), L1_LEADERS("L1_LEADERS");

    private final String name;

    ChatRoom(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}

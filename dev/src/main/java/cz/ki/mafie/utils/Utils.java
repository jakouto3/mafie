package cz.ki.mafie.utils;

import cz.ki.mafie.exception.MessageException;
import lombok.extern.slf4j.Slf4j;

import javax.annotation.Nullable;
import java.util.function.Predicate;

@Slf4j
public class Utils {

    public static String checkNullOrEmpty(String var1,@Nullable String errorMessage){
        return checkNullOrEmpty(var1, errorMessage, RuntimeException.class);
    }

    public static <T> String checkNullOrEmpty(String var1,@Nullable String errorMessage, Class<T> clazz){
        if(var1 == null || var1.trim().isEmpty()){
            log.error(errorMessage);
            if(clazz == MessageException.class){
                throw new MessageException(errorMessage);
            }else {
                throw new RuntimeException(errorMessage);
            }
        } else {
            return var1;
        }
    }

    public static <T> T checkNotNull(T value, String message){
        return checkNotNull(value, message, NullPointerException.class);
    }

    public static <T, V> T checkNotNull(T value, String message, Class<V> clazz) {
        if (value == null) {
            log.error(message);
            if(clazz == MessageException.class){
                throw new MessageException(message);
            } else{
                throw new NullPointerException(message);
            }
        } else {
            return value;
        }
    }

    //todo add parameter clazz to made it consistent with others methods
    public static <T, P> T checkNotNullOrPredicate(T value, Predicate<T> predicate, String errorMessage){
        if(value == null || predicate.test(value)){
            log.error(errorMessage);
            throw new MessageException(errorMessage);
        }else {
            return value;
        }
    }
    //todo add parameter clazz to made it consistent with others methods
    public static <T, P> T checkPredicateIfNotNull(T value, Predicate<T> predicate, String errorMessage){
        if(value != null && predicate.test(value)){
            log.error(errorMessage);
            throw new MessageException(errorMessage);
        }else {
            return value;
        }
    }

    public static String checkNullOrEmpty(String var1){
        return checkNullOrEmpty(var1, "Given string is null, empty or with only whitespaces");
    }

}

package cz.ki.mafie.exception;

public class MessageException extends RuntimeException {
    public MessageException() {
    }

    public MessageException(String s) {
        super(s);
    }
}

package cz.ki.mafie.service.impl;

import cz.ki.mafie.dto.profile.ProfileForReDto;
import cz.ki.mafie.dto.profile.team.TeamProfileForReDto;
import cz.ki.mafie.repository.profile.ProfileRepository;
import cz.ki.mafie.service.ProfileService;
import cz.ki.mafie.service.converter.ProfileConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ProfileServiceImpl implements ProfileService {

    @Autowired
    private ProfileRepository profileRepo;

    @Override
    public List<ProfileForReDto> getProfilesInRe(int realestateId) {
        return profileRepo.findAllByRealestate_Id(realestateId)
                .stream()
                .map(ProfileConverter::toDto)
                .collect(Collectors.toList());
    }
}

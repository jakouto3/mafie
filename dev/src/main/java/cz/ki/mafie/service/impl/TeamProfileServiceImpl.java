package cz.ki.mafie.service.impl;

import cz.ki.mafie.dto.profile.team.TeamProfileForReDto;
import cz.ki.mafie.repository.teamProfile.TeamProfileRepository;
import cz.ki.mafie.service.TeamProfileService;
import cz.ki.mafie.service.converter.TeamProfileConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class TeamProfileServiceImpl implements TeamProfileService {

    @Autowired
    private TeamProfileRepository teamProfileRepo;

    @Override
    public List<TeamProfileForReDto> findAll() {
        return teamProfileRepo.findAll()
                .stream()
                .map(TeamProfileConverter::toForReDto)
                .collect(Collectors.toList());
    }
}

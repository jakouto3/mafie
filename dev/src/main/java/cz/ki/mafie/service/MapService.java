package cz.ki.mafie.service;

import cz.ki.mafie.dto.map.FullMapDto;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface MapService {

    @Transactional
    FullMapDto getMapInfo();
}

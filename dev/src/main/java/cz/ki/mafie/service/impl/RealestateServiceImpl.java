package cz.ki.mafie.service.impl;

import cz.ki.mafie.domain.*;
import cz.ki.mafie.domain.enumeration.RealestateChangeType;
import cz.ki.mafie.dto.UserDto;
import cz.ki.mafie.dto.profile.team.TeamProfileForReDto;
import cz.ki.mafie.dto.realestate.ReActionDto;
import cz.ki.mafie.dto.realestate.ReDetailOverviewDto;
import cz.ki.mafie.dto.realestate.RealestateDto;
import cz.ki.mafie.dto.realestateHistoryRecord.ReHistoryRecordDto;
import cz.ki.mafie.dto.realestateHistoryRecord.ReHistoryRecordInfoDto;
import cz.ki.mafie.exception.MessageException;
import cz.ki.mafie.repository.log.LogRecordRepository;
import cz.ki.mafie.repository.profile.ProfileRepository;
import cz.ki.mafie.repository.realestate.RealestateRepository;
import cz.ki.mafie.repository.realestateHistoryRecord.RealestateHistoryRecordRepository;
import cz.ki.mafie.repository.teamProfile.TeamProfileRepository;
import cz.ki.mafie.repository.user.UserRepository;
import cz.ki.mafie.service.RealestateService;
import cz.ki.mafie.service.converter.HistoryRecordConverter;
import cz.ki.mafie.service.converter.RealestateConverter;
import cz.ki.mafie.service.converter.TeamProfileConverter;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Slf4j
public class RealestateServiceImpl implements RealestateService {

    @Autowired
    private RealestateHistoryRecordRepository reHistoryRecordRepo;

    @Autowired
    private RealestateRepository reRepo;

    @Autowired
    private ProfileRepository profileRepo;

    @Autowired
    private LogRecordRepository logRepo;

    @Autowired
    private TeamProfileRepository teamProfileRepo;

    @Autowired
    private UserRepository userRepo;

    private LogRecordDO prepareLogRecordDto(@NonNull ReActionDto reActionDto, RealestateDO realestate,@Nullable TeamProfileDO teamProfile) {
        return LogRecordDO.builder()
                .realestateChangeType(reActionDto.getAction())
                .realEstateName(realestate.getName())
                .realEstateNum(realestate.getDistrictNumber())
                .teamLabel(teamProfile == null ? null : teamProfile.getName())
                .recordedTime(LocalDateTime.now())
                .build();

    }

    @Override
    public void createRecord(@NonNull ReActionDto reActionDto, @NonNull UserDto currentUser){

        TeamProfileDO owner = reActionDto.getTeamId() == null ? null : teamProfileRepo.findById(reActionDto.getTeamId())
                .orElseThrow(() -> new RuntimeException("Team with id: " + reActionDto.getTeamId() + " doesnt exist"));
        UserDO author = userRepo.findById(currentUser.getId()).orElseThrow(() -> new RuntimeException("Cannot find current user with id: " + currentUser.getId()));

        RealestateDO realEstate = reRepo.findById(reActionDto.getRealestateId())
                .orElseThrow(() -> new RuntimeException("Specified real estate does not exist."));
        LogRecordDO logRecordDo = prepareLogRecordDto(reActionDto, realEstate, owner);

        RealestateChangeType newAction = reActionDto.getAction();
        RealestateHistoryRecordDO reHistoryRecordDo = RealestateHistoryRecordDO.builder()
                    .timeFrom(LocalDateTime.now())
                    .realestateChangeType(newAction)
                    .author(author)
                    .owner(owner)
                    .profitPerMinute(realEstate.getProfitPerMinute())
                    .realestate(realEstate)
                .build();

        Optional<RealestateHistoryRecordDO> lastRecord = reHistoryRecordRepo.findLastRecordForRe(reActionDto.getRealestateId());
        if(lastRecord.isPresent()) {
            Optional<RealestateHistoryRecordDO> lastNonProfitChangeRecord = reHistoryRecordRepo.findLastNonChangeProfitRecord(reActionDto.getRealestateId());

            createNotFirstRecord(newAction, logRecordDo, reHistoryRecordDo, lastRecord.get(), lastNonProfitChangeRecord);
        } else {
            createFirstRecord(logRecordDo, newAction, reHistoryRecordDo);
        }
    }

    private void createFirstRecord(LogRecordDO logRecordDo, RealestateChangeType newAction, RealestateHistoryRecordDO reHistoryRecordDo) {
        if (newAction.equals(RealestateChangeType.BURN)) {
            throw new MessageException("První akce nemůže být vypálení, nelze vypálit nezabranou budovu.");
        } else {
            reHistoryRecordRepo.create(reHistoryRecordDo);
            if(newAction.equals(RealestateChangeType.CLAIM)){
                logRepo.addLogRecord(logRecordDo);
            }
        }
    }

    private void createNotFirstRecord(RealestateChangeType newAction, LogRecordDO logRecordDo, RealestateHistoryRecordDO reHistoryRecordDo, RealestateHistoryRecordDO lastRecord, Optional<RealestateHistoryRecordDO> lastNonProfitChangeRecord) {
        if (lastNonProfitChangeRecord.isPresent()) {
            //exist at least one action which is NOT CHANGE_PROFIT
            RealestateChangeType lastAction = lastNonProfitChangeRecord.get().getRealestateChangeType();
            if (newAction.isOpposite(lastAction) || newAction == RealestateChangeType.CHANGE_PROFIT) {
                createAndLogRecord(newAction, logRecordDo, reHistoryRecordDo, lastRecord);
            } else {
                throw new MessageException("Akce musí být CHANGE_PROFIT(změna profitu) nebo opačná té předchozí.");
            }
        } else {
            //All previous records are just for CHANGE_PROFIT action
            if(newAction.equals(RealestateChangeType.BURN)){
                throw new MessageException("Nelze vypálit nezabranou budovu.");
            } else {
                createAndLogRecord(newAction, logRecordDo, reHistoryRecordDo, lastRecord);
            }
        }
    }

    private void createAndLogRecord(RealestateChangeType newAction, LogRecordDO logRecordDo, RealestateHistoryRecordDO reHistoryRecordDo, RealestateHistoryRecordDO lastRecord) {
        lastRecord.setTimeTo(LocalDateTime.now());
        reHistoryRecordRepo.create(reHistoryRecordDo);
        if (!newAction.equals(RealestateChangeType.CHANGE_PROFIT)) {
            logRepo.addLogRecord(logRecordDo);
        }
    }

    @Override
    public void updateReInfo(@NonNull RealestateDto realestateDto, @NonNull UserDto currentUser) {

        RealestateDO realEstate = reRepo.findById(realestateDto.getId()).orElseThrow(() -> new RuntimeException("Specified real estate does not exist."));

        int oldProfitPerMinute = realEstate.getProfitPerMinute();
        Integer newProfitPerMinute = realestateDto.getProfitPerMinute();

        //must be before prepareReActionDto, because it's getting curProfitPerMinute from found realEstate
        updateAndValidateRealestate(realestateDto, realEstate);

        if(!newProfitPerMinute.equals(oldProfitPerMinute)){
            ReActionDto reActionDto = prepareReActionDto(currentUser, realEstate.getId());
            createRecord(reActionDto, currentUser);
        }
    }

    private ReActionDto prepareReActionDto(@NonNull UserDto currentUser, int realEstateId) {
        ReActionDto.ReActionDtoBuilder reActionDtoBuilder = ReActionDto.builder()
                    .realestateId(realEstateId)
                    .action(RealestateChangeType.CHANGE_PROFIT)
                    .authorId(currentUser.getId());

        reHistoryRecordRepo.findLastRecordForRe(realEstateId).ifPresent(lastRecord -> {
                    if(lastRecord.getOwner() != null){
                        reActionDtoBuilder.teamId(lastRecord.getOwner().getId());
                    }
                });
        return reActionDtoBuilder.build();
    }

    @Override
    public ReDetailOverviewDto getReDetail(int reId) {
        Optional<RealestateDO> realestateDO = reRepo.findById(reId);
        if(!realestateDO.isPresent()) {
            throw new RuntimeException("Specified real estate does not exist.");
        }
        List<RealestateHistoryRecordDO> records = reHistoryRecordRepo.findAllByRealestate_IdOrderByTimeFromAsc(reId);
        List<ReHistoryRecordInfoDto> recordsDto = records.stream()
                .map(HistoryRecordConverter::toInfoDto)
                .collect(Collectors.toList());
        Optional<TeamProfileForReDto> owner = findCurrentOwner(reId);
        return ReDetailOverviewDto.builder()
                .realestateDto(RealestateConverter.toDto(realestateDO.get(), owner))
                .reHistoryRecords(recordsDto)
                .build();
    }

    @Override
    public void deleteRecord(int recordId){
        RealestateHistoryRecordDO record = reHistoryRecordRepo.findById(recordId)
                .orElseThrow(() -> new MessageException("Cannot find record with id: " + recordId));

        List<RealestateHistoryRecordDO> allHistRec = reHistoryRecordRepo.findAllByRealestate_IdOrderByTimeFromAsc(record.getRealestate().getId());
        RealestateHistoryRecordDO currLastRecord = allHistRec.get(allHistRec.size()-1);

        if(!this.canDeleteRecord(record, currLastRecord)) {
            throw new MessageException("You can delete only last record from history.");
        }

        if(allHistRec.size() > 1){
            RealestateHistoryRecordDO newLast = allHistRec.get(allHistRec.size()-2);
            reHistoryRecordRepo.switchToLastRecord(newLast);
        }
        reHistoryRecordRepo.deleteById(recordId);
    }

    @Override
    public boolean canDeleteRecord(int recordId) {
        Optional<RealestateHistoryRecordDO> historyRecord = reHistoryRecordRepo.findById(recordId);
        if(historyRecord.isPresent()){
            Optional<RealestateHistoryRecordDO> lastRecord = reHistoryRecordRepo.findLastRecordForRe(historyRecord.get().getRealestate().getId());
            if(!lastRecord.isPresent()){
                throw new MessageException("there is no log record yet.");
            }
            return historyRecord.get().equals(lastRecord.get());
        } else {
            throw new MessageException("specified record does not exist.");
        }
    }

    private boolean canDeleteRecord(RealestateHistoryRecordDO historyRecord, RealestateHistoryRecordDO lastRecord) {
        if(lastRecord == null){
            throw new MessageException("there is no log record yet.");
        }
        return historyRecord.equals(lastRecord);
    }

    @Override
    public boolean canBeBurned(int reId) {
        Optional<RealestateHistoryRecordDO> lastRec = reHistoryRecordRepo.findLastNonChangeProfitRecord(reId);
        return lastRec.isPresent() && lastRec.get().getRealestateChangeType().equals(RealestateChangeType.CLAIM);
    }

    @Override
    public boolean canBeClaimed(int reId) {
        Optional<RealestateHistoryRecordDO> lastRec = reHistoryRecordRepo.findLastNonChangeProfitRecord(reId);
        if(lastRec.isPresent() && lastRec.get().getRealestateChangeType().equals(RealestateChangeType.BURN)){
            return true;
        } else {
            return !lastRec.isPresent();
        }
    }

    @Override
    public List<RealestateDto> findAllByOrderByDistrictNumber() {
        return reRepo.findAllByOrderByDistrictNumber()
                .stream()
                .map(re -> RealestateConverter.toDto(re, findCurrentOwner(re.getId())))
                .collect(Collectors.toList());
    }

    @Override
    //todo missing tests
    public Optional<TeamProfileForReDto> findCurrentOwner(int realEstateId) {
        Optional<RealestateHistoryRecordDO> record = reHistoryRecordRepo.findLastNonChangeProfitRecord(realEstateId);
        if(!record.isPresent() || record.get().getRealestateChangeType() == RealestateChangeType.BURN){
            return Optional.empty();
        }

        TeamProfileDO team = record.get().getOwner();
        return Optional.of(TeamProfileConverter.toForReDto(team));
    }

    RealestateDO updateAndValidateRealestate(@NonNull RealestateDto dto, @NonNull RealestateDO reDo) {
        reDo.setPrice(dto.getPrice());
        reDo.setProfitPerMinute(dto.getProfitPerMinute());
        reDo.setName(dto.getName());
        reDo.setDistrictNumber(dto.getDistrictNumber());
        reDo.setShape(dto.getShape());

        reRepo.validate(reDo);

        return reDo;
    }

    RealestateHistoryRecordDO updateAndValidateRepositoryReRecord(@NonNull ReHistoryRecordDto dto, @NonNull RealestateHistoryRecordDO recordDO){

        ProfileDO author = profileRepo.findById(dto.getAuthorId()).orElseThrow(() -> new MessageException("Autor" + dto.getAuthorId() + " neexistuje"));
        recordDO.setAuthor(author);

        TeamProfileDO owner = teamProfileRepo.findById(dto.getOwnerId()).orElseThrow(() -> new MessageException("Vlastnici" + dto.getOwnerId() + " neexistuje"));
        recordDO.setOwner(owner);

        RealestateDO realestate = reRepo.findById(dto.getRealestateId()).orElseThrow(() -> new MessageException("Realestate" + dto.getRealestateId()+ " neexistuje"));
        recordDO.setRealestate(realestate);

        recordDO.setRealestateChangeType(dto.getActionType());
        recordDO.setTimeFrom(dto.getTimeFrom());
        recordDO.setTimeTo(dto.getTimeTo());
        recordDO.setProfitPerMinute(dto.getProfitPerMinute());

        reHistoryRecordRepo.validate(recordDO);

        return recordDO;
    }
}

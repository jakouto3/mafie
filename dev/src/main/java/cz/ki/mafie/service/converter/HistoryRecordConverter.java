package cz.ki.mafie.service.converter;

import cz.ki.mafie.domain.RealestateHistoryRecordDO;
import cz.ki.mafie.dto.realestateHistoryRecord.ReHistoryRecordDto;
import cz.ki.mafie.dto.realestateHistoryRecord.ReHistoryRecordInfoDto;
import lombok.NonNull;

public class HistoryRecordConverter {

    public static ReHistoryRecordDto toDto(@NonNull RealestateHistoryRecordDO realestateHistoryRecordDO){
        return ReHistoryRecordDto.builder()
                    .id(realestateHistoryRecordDO.getId())
                    .authorId(realestateHistoryRecordDO.getAuthor().getId())
                    .actionType(realestateHistoryRecordDO.getRealestateChangeType())
                    .profitPerMinute(realestateHistoryRecordDO.getProfitPerMinute())
                    .ownerId(realestateHistoryRecordDO.getOwner() == null ? null : realestateHistoryRecordDO.getOwner().getId())
                    .timeFrom(realestateHistoryRecordDO.getTimeFrom())
                    .timeTo(realestateHistoryRecordDO.getTimeTo())
                    .isLastRecord(realestateHistoryRecordDO.getTimeTo() == null)
                    .authorId(realestateHistoryRecordDO.getRealestate().getId())
                .build();
    }

    public static ReHistoryRecordInfoDto toInfoDto(@NonNull RealestateHistoryRecordDO realestateHistoryRecordDO){
        return ReHistoryRecordInfoDto.builder()
                    .reHistoryRecordDto(toDto(realestateHistoryRecordDO))
                    .teamLabel(realestateHistoryRecordDO.getOwner() == null ? null : realestateHistoryRecordDO.getOwner().getName())
                    .creatorName(realestateHistoryRecordDO.getAuthor().getLogin())
                .build();
    }
}

package cz.ki.mafie.service.converter;

import cz.ki.mafie.domain.RealestateDO;
import cz.ki.mafie.domain.TeamProfileDO;
import cz.ki.mafie.dto.profile.team.TeamProfileForReDto;
import cz.ki.mafie.dto.realestate.RealestateDto;
import lombok.NonNull;

import java.util.Optional;

public class RealestateConverter {

    public static RealestateDto toDto(@NonNull RealestateDO realestateDO, @NonNull Optional<TeamProfileForReDto> teamDO){
        return RealestateDto.builder()
                    .districtNumber(realestateDO.getDistrictNumber())
                    .name(realestateDO.getName())
                    .id(realestateDO.getId())
                    .price(realestateDO.getPrice())
                    .profitPerMinute(realestateDO.getProfitPerMinute())
                    .shape(realestateDO.getShape())
                    .owner(teamDO.orElse(null))
                .build();
    }

}

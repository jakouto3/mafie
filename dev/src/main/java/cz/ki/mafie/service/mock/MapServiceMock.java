package cz.ki.mafie.service.mock;

import cz.ki.mafie.dto.map.FullMapDto;
import cz.ki.mafie.dto.profile.team.TeamForMapDto;
import cz.ki.mafie.dto.realestate.RealestateDto;
import cz.ki.mafie.service.MapService;

import java.util.ArrayList;
import java.util.List;

public class MapServiceMock implements MapService {
    @Override
    public FullMapDto getMapInfo() {
        ArrayList<TeamForMapDto> teams = new ArrayList<>();
        teams.add(new TeamForMapDto(1, "Tým 1", 410250, 15648, "red"));
        teams.add(new TeamForMapDto(2, "Tým 2", 450651, 11080, "blue"));
        teams.add(new TeamForMapDto(7, "Tým 3", 215816, 2540, "yellow"));
        teams.add(new TeamForMapDto(9, "Tým 4", 430910, 60, "green"));

        RealestateServiceMock reServiceMock = new RealestateServiceMock();
        ArrayList<RealestateDto> realestates = new ArrayList<>();
        realestates.addAll(reServiceMock.findAllByOrderByDistrictNumber());

        return FullMapDto.builder().teams(teams).realEstates(realestates).build();
    }
}

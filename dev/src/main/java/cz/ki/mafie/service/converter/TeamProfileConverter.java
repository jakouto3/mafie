package cz.ki.mafie.service.converter;

import cz.ki.mafie.domain.TeamProfileDO;
import cz.ki.mafie.dto.profile.team.TeamProfileForReDto;
import lombok.NonNull;

public class TeamProfileConverter {

    public static TeamProfileForReDto toForReDto(@NonNull TeamProfileDO t){
        return TeamProfileForReDto.builder()
                .color(t.getColor())
                .name(t.getName())
                .id(t.getId())
                .build();
    }
}

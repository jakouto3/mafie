package cz.ki.mafie.service;

import cz.ki.mafie.dto.UserDto;
import cz.ki.mafie.dto.profile.team.TeamProfileForReDto;
import cz.ki.mafie.dto.realestate.ReActionDto;
import cz.ki.mafie.dto.realestate.ReDetailOverviewDto;
import cz.ki.mafie.dto.realestate.RealestateDto;
import lombok.NonNull;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 *
 */
public interface RealestateService {

    /**
     * <p>
     *  Compares ReActionDto.action with preceding action.
     *  Preceding action must be opposite to current action(every CLAIM is followed
     *  by BURN and every BURN is followed by CLAIM).
     *  First action always have to be CLAIM or CHANGE_PROFIT.
     *  </p>
     *  <p>
     *  Sets timeTo of preceding record to current time.
     *  </p>
     *  <p>
     *  Creates new ReHistoryDo with parameters from ReaActionDto and
     *  sets its timeFrom to current time.
     *  </p>
     *  <p>
     *  Creates LogRecord with appropriate parameters.
     * </p>
     * @param reActionDto Specifies parameters for new record - real estate,
     *                    action, team and author
     */
    @Transactional
    void createRecord(@NonNull ReActionDto reActionDto, @NonNull UserDto currentUser);

    /**
     * <p>
     * Updates specified parameters of RealestateDO.
     * </p>
     * <p>
     * When profitPerMinute is being updated, it creates reActionDto with
     * appropriate parameters and calls createRecord to create appropriate record.
     * </p>
     * @param realEstateDto Specifies what parameters should be updated. Parameter you
     *                      want to update is set, the rest are null.
     */
    @Transactional
    //TODO mělo by vracet ReDetailOverviewDto aby se nemusel pouzivat conventor a jen se testovala vracena hodnota
    void updateReInfo(@NonNull RealestateDto realEstateDto, @NonNull UserDto currentUser);

    /**
     * @param reId Specifies real estate you want to get information about.
     * @return complete detail information about real estate.
     */
    @Transactional(readOnly = true)
    ReDetailOverviewDto getReDetail(int reId);

    /**
     * <p>
     * Checks if is possible to delete specified record.
     * By Comparing last record for specified realestate and given record.
     * </p>
     * <p>
     * Deletes specified record.
     * </p>
     * <p>
     * Sets new last record.
     * </p>
     * @param recordId Id of record you want to delete.
     */
    @Transactional
    void deleteRecord(int recordId);

    /**
     * Only last record can be deleted.
     * @param recordId Record you want to know if is possible to delete.
     * @return True if it is possible to delete specified record, False otherwise.
     */
    @Transactional(readOnly = true)
    boolean canDeleteRecord(int recordId);

    /**
     * Real estate can be burned only if its last record is for CLAIM action.
     * @param reId Id of real estate you want to know if is possible to burn.
     * @return True if it is possible to burn specified real estate, False otherwise.
     */
    @Transactional(readOnly = true)
    boolean canBeBurned(int reId);

    /**
     * Real estate can be claimed only if its last record is for BURN action or it is
     * first record for realEstate.
     * @param reId Id of real estate you want to know if is possible to claim.
     * @return True if it is possible to claim specified real estate, False otherwise.
     */
    @Transactional(readOnly = true)
    boolean canBeClaimed(int reId);

    @Transactional(readOnly = true)
    List<RealestateDto> findAllByOrderByDistrictNumber();

    @Transactional(readOnly = true)
    Optional<TeamProfileForReDto> findCurrentOwner(int realEstateId);

}

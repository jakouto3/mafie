package cz.ki.mafie.service;

import cz.ki.mafie.dto.profile.ProfileForReDto;
import cz.ki.mafie.dto.profile.team.TeamProfileForReDto;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface ProfileService {

    @Transactional(readOnly = true)
    List<ProfileForReDto> getProfilesInRe(int realestateId);
}

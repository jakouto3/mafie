package cz.ki.mafie.service;

import cz.ki.mafie.dto.profile.team.TeamProfileForReDto;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface TeamProfileService {

    @Transactional(readOnly = true)
    List<TeamProfileForReDto> findAll();

}

package cz.ki.mafie.service.converter;

import cz.ki.mafie.domain.ProfileDO;
import cz.ki.mafie.dto.profile.ProfileForReDto;
import lombok.NonNull;

public class ProfileConverter {

    public static ProfileForReDto toDto(@NonNull ProfileDO profileDO){
        return ProfileForReDto
                .builder()
                    .gameName(profileDO.getGameName())
                    .gameRole(profileDO.getGameRole())
                    .realName(profileDO.getRealName())
                    .id(profileDO.getId())
                .build();
    }
}

package cz.ki.mafie.service.mock;

import cz.ki.mafie.domain.RealestateDO;
import cz.ki.mafie.domain.enumeration.RealestateChangeType;
import cz.ki.mafie.dto.UserDto;
import cz.ki.mafie.dto.profile.team.TeamProfileForReDto;
import cz.ki.mafie.dto.realestate.ReActionDto;
import cz.ki.mafie.dto.realestate.ReDetailOverviewDto;
import cz.ki.mafie.dto.realestate.RealestateDto;
import cz.ki.mafie.dto.realestateHistoryRecord.ReHistoryRecordDto;
import cz.ki.mafie.dto.realestateHistoryRecord.ReHistoryRecordInfoDto;
import cz.ki.mafie.exception.MessageException;
import cz.ki.mafie.service.RealestateService;
import lombok.NonNull;

import java.time.LocalDateTime;
import java.time.Month;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class RealestateServiceMock implements RealestateService {

    @Override
    public void createRecord(@NonNull ReActionDto reActionDto, @NonNull UserDto currentUser) {
        if(reActionDto.getRealestateId() == 4){
            throw new MessageException("Something wrong");
        }
    }

    @Override
    public void updateReInfo(@NonNull RealestateDto realEstateDto, @NonNull UserDto currentUser) {
        if(realEstateDto. getId() == 4){
            throw new MessageException("Something wrong");
        }
    }

    @Override
    public ReDetailOverviewDto getReDetail(int reId) {
        if(reId == 1){
            throw new RuntimeException("Specified real estate does not exist.");
        } else if(reId == 2){
            return null;
        }

        RealestateDto realestateDto = new RealestateDto().builder()
                .districtNumber(18)
                .name("Hospoda u Isaca")
                .price(15000)
                .profitPerMinute(500)
                .owner(new TeamProfileForReDto(1, "red", "Tým 1"))
                .shape(null)
                .id(reId)
                .build();

        List<ReHistoryRecordInfoDto> li = new ArrayList<ReHistoryRecordInfoDto>();
        li.add(new ReHistoryRecordInfoDto(
                new ReHistoryRecordDto(120, 500, LocalDateTime.of(2018, Month.DECEMBER, 19, 17, 00, 05),
                        LocalDateTime.of(2018, Month.DECEMBER, 19, 18, 15, 45), RealestateChangeType.CLAIM, realestateDto.getId(),
                        5, 6, false),
                "Jan Vomáčka",
                "Tým 1"
        ));
        li.add(new ReHistoryRecordInfoDto(
                new ReHistoryRecordDto(150, 500, LocalDateTime.of(2018, Month.DECEMBER, 19, 18, 15, 45),
                        null, RealestateChangeType.BURN, realestateDto.getId(), 7, 8, true),
                "Petr Richter",
                "Tým 3"
        ));

        return new ReDetailOverviewDto().builder()
                .realestateDto(realestateDto)
                .reHistoryRecords(li)
                .build();
    }

    @Override
    public void deleteRecord(int recordId) {
        if(recordId != 150){
            throw new MessageException("Cannot be deleted");
        }
    }

    @Override
    public boolean canDeleteRecord(int recordId) {
        if(recordId == 150) return true;
        return false;
    }


    @Override
    public boolean canBeBurned(int reId) {
        if (reId == 4) return false;
        return true;
    }

    @Override
    public boolean canBeClaimed(int reId) {
        if (reId == 5) return false;
        return true;
    }

    @Override
    public List<RealestateDto> findAllByOrderByDistrictNumber() {
        List<RealestateDto> li = new ArrayList<RealestateDto>();
        li.add(new RealestateDto().builder()
                .districtNumber(1)
                .name("Nové Město")
                .price(12000)
                .profitPerMinute(600)
                .owner(new TeamProfileForReDto(1, "red", "Tým 1"))
                .shape("226 285,206 293,187 294,170 290,154 284,120 262,86 237,78 231,70 219,62 204,54 197,46 193,40 192,37 195,32 201,28 203,28 209,37 222,60 249,106 295,156 334,180 351,194 363,207 377,237 422,244 441,254 437,263 436,271 436,283 438,295 446,361 371,251 272")
                .id(1)
                .build());
        li.add(new RealestateDto().builder()
                .districtNumber(2)
                .name("U kostela")
                .price(10000)
                .profitPerMinute(350)
                .shape("252 270,362 369,385 344,280 253")
                .id(2)
                .build());
        li.add(new RealestateDto().builder()
                .districtNumber(3)
                .name("Na rohu")
                .price(20000)
                .profitPerMinute(800)
                .owner(new TeamProfileForReDto(2, "blue", "Tým 2"))
                .shape("282 250,386 342,402 326,303 236")
                .id(3)
                .build());
        li.add(new RealestateDto().builder()
                .districtNumber(5)
                .name("Za kašnou")
                .price(6000)
                .profitPerMinute(150)
                .owner(new TeamProfileForReDto(9, "green", "Tým 4"))
                .shape("427 289,426 296,423 303,363 371,458 454,528 364,532 357,533 351,533 341,531 337,527 324,522 318,489 279,435 212,399 164,337 210")
                .id(4)
                .build());
        li.add(new RealestateDto().builder()
                .districtNumber(18)
                .name("Hospoda u Isaca")
                .price(15000)
                .profitPerMinute(500)
                .owner(new TeamProfileForReDto(1, "red", "Tým 1"))
                .shape("776 329,745 301,708 267,679 224,637 258,648 271,677 306,644 333,609 294,558 338,536 342,535 352,586 385,636 407,690 422,743 419,752 416,771 406,811 364")
                .id(5)
                .build());
        return li;
    }

    @Override
    public Optional<TeamProfileForReDto> findCurrentOwner(int realEstateId) {
        throw new RuntimeException("Not implemented");
    }
}

package cz.ki.mafie.repository.utils;

import cz.ki.mafie.exception.MessageException;
import lombok.extern.slf4j.Slf4j;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Slf4j
public abstract class DomainUtils {

    @PersistenceContext
    protected EntityManager em;

    @Nullable
    public <T> T getReference(Class<T> entityClass, @Nullable Integer id) {
        return id == null ? null : em.getReference(entityClass, id);
    }

    @Nonnull
    public <T> T safeFind(Class<T> entityClass, Object primaryKey) {
        return safeFind(entityClass, primaryKey, "Záznam pro entitu " + entityClass.getSimpleName() + " s id " + primaryKey + " nenalezen.");
    }

    @Nonnull
    public <T> T safeFind(Class<T> entityClass, Object primaryKey, String errorMessage) {
        T entity = em.find(entityClass, primaryKey);
        if (entity == null) {
            log.error(errorMessage);
            throw new MessageException(errorMessage);
        }

        return entity;
    }
}

package cz.ki.mafie.repository.realestate;

import cz.ki.mafie.domain.RealestateDO;
import cz.ki.mafie.dto.profile.team.TeamProfileForReDto;
import cz.ki.mafie.dto.realestate.RealestateDto;
import lombok.NonNull;

import java.util.Optional;

public interface CustomRealestateRepository {

    void validate(@NonNull RealestateDO reDo);
}

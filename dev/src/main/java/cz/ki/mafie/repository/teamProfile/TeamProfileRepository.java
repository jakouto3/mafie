package cz.ki.mafie.repository.teamProfile;

import cz.ki.mafie.domain.TeamProfileDO;
import cz.ki.mafie.repository.utils.QuerydslPredicateExecutorAdapter;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TeamProfileRepository extends JpaRepository<TeamProfileDO, Integer>, QuerydslPredicateExecutorAdapter<TeamProfileDO> {

}

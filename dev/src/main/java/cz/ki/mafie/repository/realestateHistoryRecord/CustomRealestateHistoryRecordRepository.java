package cz.ki.mafie.repository.realestateHistoryRecord;

import cz.ki.mafie.domain.RealestateHistoryRecordDO;
import cz.ki.mafie.dto.realestateHistoryRecord.ReHistoryRecordDto;
import lombok.NonNull;
import org.springframework.lang.Nullable;

import java.util.List;
import java.util.Optional;

public interface CustomRealestateHistoryRecordRepository {

    /**
     * Validates if the record consists of correct data
     * @throws  cz.ki.mafie.exception.IllegalArgumentExceptionOwn when some parameter does not meet requirements
     * @param reRecord current middle record
     */
    void validate(@NonNull RealestateHistoryRecordDO reRecord);

    /**
     * validates full consistency of the realestate records,
     * if there are no time spaces
     * <p>last record timeTo = null
     * <p>if we have A,B,C in time consistency than
     * <p>A.timeTo = B.timeFrom
     * <p>B.timeTo = C.timeFrom
     * <p>C.timeTo = null
     * @throws cz.ki.mafie.exception.IllegalArgumentExceptionOwn when inconsistency occurred
     * @param realestateId id of the realestate we want the history records test
     */
    void validateReRecordsTimeConsequence(int realestateId);


    /**
     * This method creates DO objects via with several constrains
     * adds only to an end
     * @throws cz.ki.mafie.exception.IllegalArgumentExceptionOwn when some parameter does not meet requirements
     * @param recordDO the dto class representing the data the DO is created from
     * @return new DO object
     */
    @NonNull
    RealestateHistoryRecordDO create(@NonNull RealestateHistoryRecordDO recordDO);


    /**
     * Finds all records of given realestate
     * @param realestateId id of the looked fo realestate
     * @return list of all realestate records
     */
    List<RealestateHistoryRecordDO> findAllByRealestate_IdOrderByTimeFromAsc(int realestateId);

    /**
     * Finds the last record of given realestate
     * @param realestateId id of the realestate
     * @return last record wrapped in Optional class
     */
    Optional<RealestateHistoryRecordDO> findLastRecordForRe(int realestateId);

    /**
     * sets RecordsDo timeTo = null
     * @param recordDO record to be set
     * @deprecated should be implement in service layer
     */
    @Deprecated
    void switchToLastRecord(@NonNull RealestateHistoryRecordDO recordDO);


    Optional<RealestateHistoryRecordDO> findLastNonChangeProfitRecord (int reId);


}

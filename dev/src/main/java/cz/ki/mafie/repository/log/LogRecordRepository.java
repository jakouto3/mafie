package cz.ki.mafie.repository.log;

import cz.ki.mafie.domain.LogRecordDO;
import cz.ki.mafie.repository.utils.QuerydslPredicateExecutorAdapter;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.logging.LogRecord;

public interface LogRecordRepository extends JpaRepository<LogRecordDO, Integer>, QuerydslPredicateExecutorAdapter<LogRecordDO>, CustomLogRecordRepository {


}
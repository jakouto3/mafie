package cz.ki.mafie.repository.generalProfile;

import com.querydsl.jpa.impl.JPAQuery;
import cz.ki.mafie.domain.GeneralProfileDO;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

import static cz.ki.mafie.domain.Q.Q_GEN_PROFILE;

public class GeneralProfileRepositoryImpl implements CustomGeneralProfileRepository {

    @PersistenceContext
    private EntityManager em;

    @Override
    public List<GeneralProfileDO> getProfilesLocatedInRe(int realestateId) {
        return new JPAQuery<GeneralProfileDO>(em)
                .select(Q_GEN_PROFILE)
                .from(Q_GEN_PROFILE)
                .where(Q_GEN_PROFILE.realestate.id.eq(realestateId))
                .fetch();
    }
}

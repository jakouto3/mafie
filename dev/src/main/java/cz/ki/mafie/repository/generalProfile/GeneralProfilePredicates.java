package cz.ki.mafie.repository.generalProfile;

import com.querydsl.core.types.Predicate;

import static cz.ki.mafie.domain.Q.Q_GEN_PROFILE;

public class GeneralProfilePredicates {

    public static Predicate generalProfileLocatedIn(int realestateId){
        return Q_GEN_PROFILE.realestate.id.eq(realestateId);
    }
}

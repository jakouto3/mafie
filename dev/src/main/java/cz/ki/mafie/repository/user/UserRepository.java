package cz.ki.mafie.repository.user;

import cz.ki.mafie.domain.UserDO;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<UserDO, Integer> {
}

package cz.ki.mafie.repository.realestateHistoryRecord;

import com.google.common.collect.Iterables;
import com.querydsl.jpa.impl.JPAQuery;
import cz.ki.mafie.domain.*;
import cz.ki.mafie.domain.enumeration.RealestateChangeType;
import cz.ki.mafie.dto.realestateHistoryRecord.ReHistoryRecordDto;
import cz.ki.mafie.exception.MessageException;
import cz.ki.mafie.repository.utils.DomainUtils;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;

import java.util.List;
import java.util.Optional;

import static cz.ki.mafie.domain.Q.Q_RE_HISTORY_RECORD;
import static cz.ki.mafie.utils.Utils.checkNotNull;
import static cz.ki.mafie.utils.Utils.checkNotNullOrPredicate;
import static cz.ki.mafie.utils.Utils.checkPredicateIfNotNull;

@Slf4j
public class CustomRealestateHistoryRecordRepositoryImpl extends DomainUtils implements CustomRealestateHistoryRecordRepository {

    @Override
    public void validate(@NonNull RealestateHistoryRecordDO reRecord){

        checkNotNullOrPredicate(reRecord.getProfitPerMinute(), profit -> profit < 0, "Profit za minutu musí být zadán a nesmí být záporný");
        checkNotNull(reRecord.getTimeFrom(),"Čas začátku zalogování musí být nastaven");
        //TODO metoda vyhazuje message exception ale mela by vyhazovat runtime
        checkPredicateIfNotNull(reRecord.getTimeTo(),time -> time.isBefore(reRecord.getTimeFrom()),"Čas ukončení nesmí být menší než čas začátku" );
        checkNotNull(reRecord.getRealestate(), "Realestate musí být nastaven");
        checkNotNull(reRecord.getRealestateChangeType(),"Akce musí být vyplněna");
        checkNotNull(reRecord.getAuthor(),"Autor musí být vyplněn");
    }

    @Override
    public void validateReRecordsTimeConsequence(int realestateId) {
        List<RealestateHistoryRecordDO> records = findAllByRealestate_IdOrderByTimeFromAsc(realestateId);

        RealestateHistoryRecordDO prev = null;

        for(RealestateHistoryRecordDO r : records){
            if(prev != null && !prev.getTimeTo().isEqual(r.getTimeFrom())){
                throw new RuntimeException("Records are not in valid state, hole exists");
            }
            prev = r;
        }

        RealestateHistoryRecordDO lastRecord = Iterables.getLast(records, null);
        if(lastRecord != null && lastRecord.getTimeTo() != null){
            throw new RuntimeException("Recoords are not in valid state, last timeTO not null");
        }
    }

    @Override
    public RealestateHistoryRecordDO create(@NonNull RealestateHistoryRecordDO recordDO) {
        validate(recordDO);

        em.persist(recordDO);
        log.debug("Created " + recordDO);

        return recordDO;
    }

    @Override
    public List<RealestateHistoryRecordDO> findAllByRealestate_IdOrderByTimeFromAsc(int realestateId) {
        //I hope there will be no problem with later fetching author and owner
        //cannot use earlier innerJoin, because owner was nullable, so it join nothing
        return new JPAQuery<RealestateHistoryRecordDO>(em)
            .select(Q_RE_HISTORY_RECORD)
            .from(Q_RE_HISTORY_RECORD)
            .where(Q_RE_HISTORY_RECORD.realestate.id.eq(realestateId))
            .orderBy(Q_RE_HISTORY_RECORD.timeFrom.asc())
            .fetch();
    }

    @Override
    public Optional<RealestateHistoryRecordDO> findLastRecordForRe(int realestateId) {
        List<RealestateHistoryRecordDO> records = findAllByRealestate_IdOrderByTimeFromAsc(realestateId);

        return Optional.ofNullable(Iterables.getLast(records, null));
    }

    @Override
    public void switchToLastRecord(@NonNull RealestateHistoryRecordDO recordDO) {
        recordDO.setTimeTo(null);
    }

    @Override
    public Optional<RealestateHistoryRecordDO> findLastNonChangeProfitRecord(int reId) {
        return this.findAllByRealestate_IdOrderByTimeFromAsc(reId)
                .stream()
                .filter(record -> record.getRealestateChangeType() != RealestateChangeType.CHANGE_PROFIT)
                .reduce((fst, snd) -> snd);
    }
}

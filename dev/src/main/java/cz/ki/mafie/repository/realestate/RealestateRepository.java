package cz.ki.mafie.repository.realestate;

import cz.ki.mafie.domain.RealestateDO;
import cz.ki.mafie.repository.utils.QuerydslPredicateExecutorAdapter;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;


public interface RealestateRepository extends JpaRepository<RealestateDO, Integer>, CustomRealestateRepository, QuerydslPredicateExecutorAdapter<RealestateDO> {

    //todo smoke test
    List<RealestateDO> findAllByOrderByDistrictNumber();
}

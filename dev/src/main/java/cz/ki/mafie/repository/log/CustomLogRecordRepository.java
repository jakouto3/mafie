package cz.ki.mafie.repository.log;

import cz.ki.mafie.domain.LogRecordDO;
import cz.ki.mafie.dto.logRecord.LogRecordDto;
import lombok.NonNull;

public interface CustomLogRecordRepository {



    LogRecordDO addLogRecord(@NonNull LogRecordDO logDO);

}

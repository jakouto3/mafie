package cz.ki.mafie.repository.log;

import cz.ki.mafie.domain.LogRecordDO;
import cz.ki.mafie.dto.logRecord.LogRecordDto;
import cz.ki.mafie.exception.MessageException;
import cz.ki.mafie.repository.utils.DomainUtils;
import cz.ki.mafie.utils.Utils;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import static cz.ki.mafie.utils.Utils.checkNotNull;
import static cz.ki.mafie.utils.Utils.checkNotNullOrPredicate;

@Slf4j
public class LogRecordRepositoryImpl extends DomainUtils implements CustomLogRecordRepository {

    @PersistenceContext
    private EntityManager em;

    void validate(@NonNull LogRecordDO logDO){
        Utils.checkNullOrEmpty(logDO.getRealEstateName(), "LogRecord description must be filled!");
        checkNotNull(logDO.getRecordedTime(),"LogRecord description must be filled!");
        Utils.checkNullOrEmpty(logDO.getTeamLabel(), "LogRecord description must be filled!");
        checkNotNull(logDO.getRealestateChangeType(),"LogRecord description must be filled!");
        checkNotNullOrPredicate(logDO.getRealEstateNum(),reNum -> reNum < 0, "Realestate num must be positive" );
    }

    @Override
    public LogRecordDO addLogRecord(@NonNull LogRecordDO logDO) {
        validate(logDO);

        em.persist(logDO);
        log.debug("Created " + logDO);

        return logDO;
    }
}

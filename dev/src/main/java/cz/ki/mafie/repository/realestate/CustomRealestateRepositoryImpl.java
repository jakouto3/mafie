package cz.ki.mafie.repository.realestate;


import cz.ki.mafie.domain.RealestateDO;
import cz.ki.mafie.domain.RealestateHistoryRecordDO;
import cz.ki.mafie.domain.TeamProfileDO;
import cz.ki.mafie.domain.enumeration.RealestateChangeType;
import cz.ki.mafie.dto.profile.team.TeamProfileForReDto;
import cz.ki.mafie.dto.realestate.RealestateDto;
import cz.ki.mafie.exception.MessageException;
import cz.ki.mafie.repository.realestateHistoryRecord.RealestateHistoryRecordRepository;
import cz.ki.mafie.repository.utils.DomainUtils;
import cz.ki.mafie.service.converter.TeamProfileConverter;
import cz.ki.mafie.utils.Utils;
import lombok.NonNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.rmi.CORBA.Util;
import java.util.Optional;

import static cz.ki.mafie.utils.Utils.checkNotNullOrPredicate;
import static cz.ki.mafie.utils.Utils.checkNullOrEmpty;

@Repository
public class CustomRealestateRepositoryImpl extends DomainUtils implements CustomRealestateRepository  {

    @Override
    public void validate(@NonNull RealestateDO reDo) {
        checkNotNullOrPredicate(reDo.getPrice(), price -> price < 0, "Cena nemovitosti musí být zadána a nesmí být záporná");
        checkNotNullOrPredicate(reDo.getProfitPerMinute(), profit -> profit < 0, "Profit za minutu musí být zadán a nesmí být záporný");
        checkNullOrEmpty(reDo.getShape(), "Tvar nemovitosti musí být zadán", MessageException.class);
        checkNotNullOrPredicate(reDo.getDistrictNumber(), num -> num < 0, "Číslo nemovitosti musí být zadáno a nesmí být záporné");
        checkNullOrEmpty(reDo.getName(), "Jméno nemovitosti musí být zadáno.", MessageException.class);
    }
}

package cz.ki.mafie.repository.profile;

import cz.ki.mafie.domain.ProfileDO;
import cz.ki.mafie.repository.utils.QuerydslPredicateExecutorAdapter;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ProfileRepository extends JpaRepository<ProfileDO, Integer>, QuerydslPredicateExecutorAdapter<ProfileDO> {

    List<ProfileDO> findAllByRealestate_Id(int realestateId);
}

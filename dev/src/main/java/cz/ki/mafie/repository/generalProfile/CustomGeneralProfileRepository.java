package cz.ki.mafie.repository.generalProfile;

import cz.ki.mafie.domain.GeneralProfileDO;

import java.util.List;

public interface CustomGeneralProfileRepository {
    List<GeneralProfileDO> getProfilesLocatedInRe(int realestateId);
}

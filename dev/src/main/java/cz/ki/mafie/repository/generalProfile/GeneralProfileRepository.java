package cz.ki.mafie.repository.generalProfile;

import cz.ki.mafie.domain.GeneralProfileDO;
import cz.ki.mafie.repository.utils.QuerydslPredicateExecutorAdapter;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface GeneralProfileRepository extends JpaRepository<GeneralProfileDO, Integer>, QuerydslPredicateExecutorAdapter<GeneralProfileDO>, CustomGeneralProfileRepository {

    //todo we will move this logic to profileDO, in RE info will be displayed only profiles not team profiles
    @Deprecated
    List<GeneralProfileDO> findAllByRealestate_Id(int realestateId);

}

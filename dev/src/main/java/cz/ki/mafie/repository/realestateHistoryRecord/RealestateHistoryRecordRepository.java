package cz.ki.mafie.repository.realestateHistoryRecord;


import cz.ki.mafie.domain.RealestateHistoryRecordDO;
import cz.ki.mafie.repository.utils.QuerydslPredicateExecutorAdapter;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface RealestateHistoryRecordRepository extends JpaRepository<RealestateHistoryRecordDO, Integer>, CustomRealestateHistoryRecordRepository, QuerydslPredicateExecutorAdapter<RealestateHistoryRecordDO>{
}

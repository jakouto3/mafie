package cz.ki.mafie.repository.utils;

import com.google.common.collect.Lists;
import com.querydsl.core.types.OrderSpecifier;
import com.querydsl.core.types.Predicate;
import org.springframework.data.domain.Sort;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

import java.util.List;

public interface QuerydslPredicateExecutorAdapter<T> extends QuerydslPredicateExecutor<T> {

    default List<T> findAllToList(Predicate var1){
        return Lists.newArrayList(findAll(var1));
    }

    default List<T> findAllToList(Predicate var1, Sort var2){
        return Lists.newArrayList(findAll(var1, var2));
    }

    default List<T> findAllToList(Predicate var1, OrderSpecifier... var2){
        return Lists.newArrayList(findAll(var1, var2));
    }

    default List<T> findAllToList(OrderSpecifier... var1){
        return Lists.newArrayList(findAll(var1));
    }
}

package cz.ki.mafie.dto.realestate;

import cz.ki.mafie.dto.AbstractDto;
import cz.ki.mafie.dto.profile.team.TeamProfileForReDto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RealestateDto extends AbstractDto {

    private Integer districtNumber;
    private String name;
    private Integer price;
    private Integer profitPerMinute;
    private TeamProfileForReDto owner;
    private String shape;

    public RealestateDto() {
    }

    @Builder
    public RealestateDto(Integer id, Integer districtNumber, String name, Integer price, Integer profitPerMinute, TeamProfileForReDto owner, String shape) {
        super(id);
        this.districtNumber = districtNumber;
        this.name = name;
        this.price = price;
        this.profitPerMinute = profitPerMinute;
        this.owner = owner;
        this.shape = shape;
    }
}

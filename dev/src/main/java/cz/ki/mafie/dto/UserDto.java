package cz.ki.mafie.dto;

import cz.ki.mafie.domain.enumeration.SystemRole;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Getter @Setter @EqualsAndHashCode(callSuper = true)
public class UserDto extends AbstractDto {

    private String username;
    private SystemRole roles;

    @Builder
    public UserDto(Integer id, String username, SystemRole roles) {
        super(id);
        this.username = username;
        this.roles = roles;
    }

    public UserDto() {
    }
}

package cz.ki.mafie.dto.map;

import cz.ki.mafie.dto.profile.team.TeamForMapDto;
import cz.ki.mafie.dto.realestate.RealestateDto;
import lombok.*;

import java.util.List;

@Getter
@Setter
@EqualsAndHashCode
@Builder
public class FullMapDto {
    @Singular
    private List<RealestateDto> realEstates;
    @Singular
    private List<TeamForMapDto> teams;
}

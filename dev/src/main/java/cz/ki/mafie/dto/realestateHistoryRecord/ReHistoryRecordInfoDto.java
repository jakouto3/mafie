package cz.ki.mafie.dto.realestateHistoryRecord;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
@AllArgsConstructor
public class ReHistoryRecordInfoDto {
    private ReHistoryRecordDto reHistoryRecordDto;
    private String creatorName;
    private String teamLabel;

    ReHistoryRecordInfoDto() {
    }
}

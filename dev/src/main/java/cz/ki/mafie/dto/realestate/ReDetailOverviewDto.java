package cz.ki.mafie.dto.realestate;

import cz.ki.mafie.dto.realestateHistoryRecord.ReHistoryRecordInfoDto;
import lombok.*;

import java.util.List;

@Getter
@Setter
@Builder
@AllArgsConstructor
public class ReDetailOverviewDto {
    private RealestateDto realestateDto;

    @Singular
    private List<ReHistoryRecordInfoDto> reHistoryRecords;

    public ReDetailOverviewDto () {

    }
}

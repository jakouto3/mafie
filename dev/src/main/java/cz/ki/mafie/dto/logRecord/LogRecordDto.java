package cz.ki.mafie.dto.logRecord;

import cz.ki.mafie.domain.enumeration.RealestateChangeType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter @Setter @Builder @AllArgsConstructor
public class LogRecordDto {

    private String realEstateName;
    private Integer realEstateNum;
    private LocalDateTime recordedTime;
    private String teamLabel;
    private RealestateChangeType realestateChangeType;

    public LogRecordDto() {
    }

}

package cz.ki.mafie.dto.profile;

import cz.ki.mafie.dto.AbstractDto;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

/**
 * Info about user located in realestate
 * */
@Getter @Setter @EqualsAndHashCode(callSuper = true)
public class ProfileForReDto extends AbstractDto {

    /**
     * Null for others than profile
     * */
    private String realName;

    /**
     * For profile is gameName for team is team label
     * */
    private String gameName;

    private String gameRole;

    @Builder
    public ProfileForReDto(Integer id, String realName, String gameName, String gameRole) {
        super(id);
        this.realName = realName;
        this.gameName = gameName;
        this.gameRole = gameRole;
    }
}

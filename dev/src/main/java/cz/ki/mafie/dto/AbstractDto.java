package cz.ki.mafie.dto;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Getter @Setter @EqualsAndHashCode
public class AbstractDto {
    protected Integer id;

    @Builder(builderMethodName = "abstractBuilder")
    public AbstractDto(Integer id) {
        this.id = id;
    }

    public AbstractDto() {
    }
}

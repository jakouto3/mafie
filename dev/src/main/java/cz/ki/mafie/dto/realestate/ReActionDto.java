package cz.ki.mafie.dto.realestate;

import cz.ki.mafie.domain.enumeration.RealestateChangeType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter @Setter @Builder @AllArgsConstructor
public class ReActionDto {

    private Integer realestateId;
    private RealestateChangeType action;
    private Integer authorId;
    private Integer teamId;

    ReActionDto(){

    }

}

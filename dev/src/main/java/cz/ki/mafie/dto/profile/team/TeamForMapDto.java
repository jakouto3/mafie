package cz.ki.mafie.dto.profile.team;

import cz.ki.mafie.dto.AbstractDto;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Getter @Setter @EqualsAndHashCode(callSuper = true)
public class TeamForMapDto extends AbstractDto {
    private String name;
    /**
     * sum of gained money from REs and manual from profile
     * */
    private Integer curMoney;
    private Integer curProfitPerMinute;
    private String color;

    public TeamForMapDto(Integer id, String name, Integer curMoney, Integer curProfitPerMinute, String color) {
        super(id);
        this.name = name;
        this.curMoney = curMoney;
        this.curProfitPerMinute = curProfitPerMinute;
        this.color = color;
    }

    public TeamForMapDto() {
    }
}

package cz.ki.mafie.dto.profile.team;


import cz.ki.mafie.dto.AbstractDto;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Getter @Setter @EqualsAndHashCode(callSuper = true)
public class TeamProfileForReDto extends AbstractDto {

    private String color;
    private String name;

    @Builder
    public TeamProfileForReDto(Integer id, String color, String name) {
        super(id);
        this.color = color;
        this.name = name;
    }

    public TeamProfileForReDto() {
    }
}

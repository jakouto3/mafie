package cz.ki.mafie.dto.realestateHistoryRecord;


import cz.ki.mafie.domain.ProfileDO;
import cz.ki.mafie.domain.RealestateDO;
import cz.ki.mafie.domain.TeamProfileDO;
import cz.ki.mafie.domain.enumeration.RealestateChangeType;
import cz.ki.mafie.dto.AbstractDto;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
public class ReHistoryRecordDto extends AbstractDto {

    private Integer profitPerMinute;
    private LocalDateTime timeFrom;
    private LocalDateTime timeTo;
    private RealestateChangeType actionType;
    private Integer realestateId;

    /**
     * id of TeamProfile responsible for this record
     * */
    private Integer ownerId;

    /**
     * id of Profile creating this record
     * */
    private Integer authorId;

    private boolean isLastRecord;

    @Builder
    public ReHistoryRecordDto(Integer id, Integer profitPerMinute, LocalDateTime timeFrom, LocalDateTime timeTo, RealestateChangeType actionType, Integer realestateId, Integer ownerId, Integer authorId, boolean isLastRecord) {
        super(id);
        this.profitPerMinute = profitPerMinute;
        this.timeFrom = timeFrom;
        this.timeTo = timeTo;
        this.actionType = actionType;
        this.realestateId = realestateId;
        this.ownerId = ownerId;
        this.authorId = authorId;
        this.isLastRecord = isLastRecord;
    }
}

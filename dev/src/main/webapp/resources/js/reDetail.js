window.onload=function(){
    document.getElementById("editButton").addEventListener("click", function(){
        document.getElementById("re-info").classList.add("d-none");
        document.getElementById("re-edit").classList.remove("d-none");
    });
    document.getElementById("cancelButton").addEventListener("click", function(){
        document.getElementById("re-edit").classList.add("d-none");
        document.getElementById("re-info").classList.remove("d-none");
    });
    var elements = document.getElementsByClassName("alert");
    //TODO if will be used in future, is neccesery to repair this component, it should clean concrete message after n seconds or on click before timeout
    for(i = 0; i < elements.length; i++){
        setTimeout(function () {
            var node = document.getElementById("messages");
            while (node.firstChild) {
                node.removeChild(node.firstChild);
            }
        }, 5000);
        elements[i].addEventListener("click", function(){
            var node = document.getElementById("messages");
            while (node.firstChild) {
                node.removeChild(node.firstChild);
            }
        });
    }


}
var interval = 60; //in s
refreshInterval = 1; //in s

console.log("start");
reload();
setInterval("reload()", interval*1000);

var timeouts = [];

function reload() {
    var request = new XMLHttpRequest();
    request.addEventListener("load", e => {
        if(e.target.status = 200){
            console.log(e);

            var jsonObject = JSON.parse(e.target.responseText);

            //clear district
            var elements = document.getElementsByTagName("polygon");
            while(elements.length>0) {
                elements[0].parentNode.removeChild(elements[0]);
            }

            //handle district
            var districts = jsonObject['realEstates'];
            var map = document.getElementById("map");
            for(i = 0; i < districts.length; i++){
                var district = districts[i];
                var owner = district['owner'];
                if (owner == null) continue;

                var newElem = document.createElementNS("http://www.w3.org/2000/svg", "polygon");
                newElem.setAttributeNS(null,"id", "shape"+district['districtNumber']);
                newElem.setAttributeNS(null,"points", district['shape']);
                newElem.setAttributeNS(null, "fill", owner['color']);
                console.log(newElem)
                map.appendChild(newElem);
            }


            //clear timeout
            for (var i = 0; i < timeouts.length; i++) {
                clearTimeout(timeouts[i]);
            }

            //handle score
            var teams = jsonObject['teams'];
            for(i = 0; i < teams.length; i++){
                var team = teams[i];
                setMoney(team['id'], team['curMoney'], (team['curProfitPerMinute']/60)*refreshInterval, refreshInterval*1000);
            }
        }
    });
    request.addEventListener("error", e => {
        console.log(e);
    });
    request.open("GET", "data/", true);
    request.send();
}

function setMoney(id, curMoney, addMoney, interval){
    var newBalance = curMoney + addMoney;
    var element = document.getElementById("profit-"+id);
    element.innerText = Math.round(newBalance) + " $";
    timeouts.push(setTimeout(setMoney, interval, id, newBalance, addMoney, interval));
}
<%--
  Created by IntelliJ IDEA.
  User: stejs_000
  Date: 29.12.2018
  Time: 18:09
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Error 500</title>

    <jsp:include page="head.jsp" />
</head>
<body>
    <div class="container">
        <h1>Chyba serveru</h1>
        <h2>Error 500</h2>
        <p><hr></p>
        <h3>Typ chyby</h3>
        <p>${message}</p>
    </div>
</body>
</html>

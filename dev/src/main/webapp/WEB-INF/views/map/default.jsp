<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
    <title>${title}</title>

    <jsp:include page="../head.jsp" />
    <script src="<c:url value="/resources/js/map.js" />"></script>
    <style>
        polygon{
            opacity: 0.5;
        }
    </style>
</head>
<body>
    <div style="max-width: 1280px; max-height: 720px" class="m-auto">
        <svg id="map" xmlns="http://www.w3.org/2000/svg" version="1.1" width="100%" viewBox="0 0 1280 720">
            <image xlink:href="<c:url value="/resources/images/map.png" />" width="1280" height="720"/>

            <c:forEach items="${realestates}" var="re">
                <c:if test="${not empty re.getOwner()}">
                    <polygon id="shape${re.getDistrictNumber()}" points="${re.getShape()}" fill="${re.getOwner().getColor()}"/>
                </c:if>
            </c:forEach>
        </svg>
    </div>
    <div class="container">
        <div class="row">
            <c:forEach items="${teams}" var="team">
                <div class="col-6 col-md-3 p-3">
                    <div style="border: 1px solid black; border-radius: 6px; background: lightgray; color: white; font-weight: bold; font-size: 20px;" class="p-2">
                        <div class="row">
                            <div class="col text-left" style="color: ${team.getColor()};">
                                    ${team.getName()}
                            </div>
                        </div>
                        <div class="row">
                            <div class="col text-center" id="profit-${team.getId()}">
                                    ${team.getCurMoney()} $
                            </div>
                        </div>
                    </div>
                </div>
            </c:forEach>
        </div>
    </div>
</body>
</html>

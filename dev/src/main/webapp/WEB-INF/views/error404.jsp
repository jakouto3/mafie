<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Error 404</title>

    <jsp:include page="head.jsp" />
</head>
<body>
    <div class="container">
        <h1>Požadovaná stránka nenalezena</h1>
        <h2>Error 404</h2>
        <hr>
    </div>
</body>
</html>

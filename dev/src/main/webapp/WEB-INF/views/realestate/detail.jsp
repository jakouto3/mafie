<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
    <title>${title}</title>

    <jsp:include page="../head.jsp" />
    <script src="<c:url value="/resources/js/reDetail.js" />"></script>
</head>
<body>
    <div id="messages" style="position: absolute; width: 100%">
        <c:if test="${not empty message}">
            <div class="alert ${message_type} text-center" role="alert">
                    ${message}
            </div>
        </c:if>
    </div>
    <div class="container">
        <a href="<c:url value="/realestate/" />">zpět</a>
        <h1>Detail nemovitosti</h1>
        <div style="border: solid black 1px; padding:16px" class="mb-3">
            <div class="row">
                <div class="col-12"><h4>${re.getDistrictNumber()} - ${re.getName()}</h4></div>
            </div>
            <div class="row">
                <div class="col-md-10 col-sm-9 col-8">
                    <div id="re-info" class="">
                        <div class="row">
                            <div class="col-md-2 col-sm-3 col-5 font-weight-bold">Cena:</div>
                            <div class="col-md-10 col-sm-9 col-7">${re.getPrice()}</div>
                        </div>
                        <div class="row">
                            <div class="col-md-2 col-sm-3 col-5 font-weight-bold">Zisk/min:</div>
                            <div class="col-md-10 col-sm-9 col-7">${re.getProfitPerMinute()}</div>
                        </div>
                        <div class="row">
                            <div class="col-md-2 col-sm-3 col-5 font-weight-bold">Vlastník:</div>
                            <div class="col-md-10 col-sm-9 col-7">${re.getOwner().getName()}<c:if test="${empty re.getOwner().getName()}">-</c:if></div>
                        </div>
                        <button type="button" class="btn btn-primary mt-2" id="editButton">Editovat</button>
                    </div>
                    <div id="re-edit" class="d-none">
                        <form action="<c:url value="/realestate/detail/edit/${re.getId()}" />" method="post">
                            <div class="row">
                                <div class="col-8">
                                    <div class="form-group row">
                                        <label for="price" class="col-sm-4 col-form-label font-weight-bold">Cena:</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" id="price" name="price" value="${re.getPrice()}">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="profit" class="col-sm-4 col-form-label font-weight-bold">Zisk/min:</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" id="profit" name="profit" value="${re.getProfitPerMinute()}">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-4 d-flex flex-column">
                                    <div><button type="button" class="btn btn-secondary m-1" id="cancelButton">Zrušit</button></div>
                                    <div><button type="submit" class="btn btn-primary m-1" id="saveButton">Uložit</button></div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-md-2 col-sm-3 col-4 d-flex flex-column justify-content-between align-items-end">
                    <div>
                        <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#sellModal">Prodat</button>
                        <!-- Modal -->
                        <div class="modal fade" id="sellModal" tabindex="-1" role="dialog" aria-labelledby="sellModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="sellModalLabel">Vyberte kupující tým</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <form action="<c:url value="/realestate/detail/sell/${re.getId()}" />" method="post">
                                        <div class="modal-body">
                                            <div class="input-group mb-3">
                                                <select class="custom-select" name="teamId">
                                                    <option selected>Vybrat...</option>
                                                    <c:forEach items="${teams}" var="team">
                                                        <option value="${team.getId()}">${team.getName()}</option>
                                                    </c:forEach>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-danger" data-dismiss="modal">ZRUŠIT</button>
                                            <button type="submit" class="btn btn-success">PRODAT</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div>
                        <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#burnModal">Vypálit</button>
                        <!-- Modal -->
                        <div class="modal fade" id="burnModal" tabindex="-1" role="dialog" aria-labelledby="burnModalLabel" aria-hidden="true">
                            <div class="modal-dialog bg-warning" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="burnModalLabel">Vyberte tým, který budovu vypálí</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <form action="<c:url value="/realestate/detail/burn/${re.getId()}" />" method="post">
                                        <div class="modal-body">
                                            <div class="input-group mb-3">
                                                <select class="custom-select" name="teamId">
                                                    <option selected>Vybrat...</option>
                                                    <c:forEach items="${teams}" var="team">
                                                        <option value="${team.getId()}">${team.getName()}</option>
                                                    </c:forEach>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-danger" data-dismiss="modal">ZRUŠIT</button>
                                            <button type="submit" class="btn btn-success">VYPÁLIT</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <h3>Historie budovy</h3>
        <div class="table-responsive">
            <table class="table table-striped">
                <thead class="thead-light">
                    <tr>
                        <th>Id</th>
                        <th>Od</th>
                        <th>Do</th>
                        <th>Akce</th>
                        <th>Tým</th>
                        <th>Autor</th>
                        <th>Zisk/min</th>
                        <th>Akce</th>
                    </tr>
                </thead>
                <tbody>
                    <c:forEach items="${historyRecords}" var="record">
                        <tr>
                            <td>${record.getReHistoryRecordDto().getId()}</td>
                            <td>
                                <fmt:parseDate value="${record.getReHistoryRecordDto().getTimeFrom()}" pattern="yyyy-MM-dd'T'HH:mm" var="parsedDateTime" type="both" />
                                <fmt:formatDate pattern="HH:mm" value="${ parsedDateTime }" />
                            </td>
                            <td>
                                <fmt:parseDate value="${record.getReHistoryRecordDto().getTimeTo()}" pattern="yyyy-MM-dd'T'HH:mm" var="parsedDateTime" type="both" />
                                <fmt:formatDate pattern="HH:mm" value="${ parsedDateTime }" />
                                <c:if test="${empty record.getReHistoryRecordDto().getTimeTo()}">-</c:if>
                            </td>
                            <td>
                                <c:set var = "value" value = "${record.getReHistoryRecordDto().actionType.toString()}"/>
                                <c:choose>
                                    <c:when test="${value.equals('BURN')}">Vypálení</c:when>
                                    <c:when test="${value.equals('CLAIM')}">Držení</c:when>
                                    <c:when test="${value.equals('CHANGE_PROFIT')}">Změna profitu</c:when>
                                    <c:otherwise>${record.getReHistoryRecordDto().actionType.toString()}</c:otherwise>
                                </c:choose>
                            </td>
                            <td>${record.getTeamLabel()}</td>
                            <td>${record.getCreatorName()}</td>
                            <td>${record.getReHistoryRecordDto().getProfitPerMinute()}</td>
                            <td><c:if test="${record.getReHistoryRecordDto().isLastRecord()}"><a href="" data-toggle="modal" data-target="#deleteHistoryRecordModal${record.getReHistoryRecordDto().getId()}">smazat</a></c:if></td>

                            <!-- Modal -->
                            <c:if test="${record.getReHistoryRecordDto().isLastRecord()}">
                            <div class="modal fade" id="deleteHistoryRecordModal${record.getReHistoryRecordDto().getId()}" tabindex="-1" role="dialog" aria-labelledby="deleteHistoryRecordModalLabel${record.getReHistoryRecordDto().getId()}" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="deleteHistoryRecordModalLabel${record.getReHistoryRecordDto().getId()}">Opravdu chcete vymazat záznam v historii?</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-danger" data-dismiss="modal">NE</button>
                                            <a href="<c:url value="/realestate/detail/${re.getId()}/deleteRecord/${record.getReHistoryRecordDto().getId()}" />" class="btn btn-success">ANO</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            </c:if>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
        </div>

    </div>
</body>
</html>

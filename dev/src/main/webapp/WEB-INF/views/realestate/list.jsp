<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
    <title>${title}</title>

    <jsp:include page="../head.jsp" />
</head>
<body>
    <div class="container">
        <h1>Nemovitosti</h1>
        <div style="border: solid black 1px; padding:16px" class="mb-3">
            <div class="table-responsive">
                <table class="table table-striped">
                    <thead class="thead-light">
                    <tr>
                        <th>Číslo</th>
                        <th>Název</th>
                        <th>Cena</th>
                        <th>Zisk / min</th>
                        <th>Vlastník</th>
                        <th>Akce</th>
                    </tr>
                    </thead>
                    <tbody>
                        <c:forEach items="${realestates}" var="re">
                            <tr>
                                <td>${re.getDistrictNumber()}</td>
                                <td>${re.getName()}</td>
                                <td>${re.getPrice()}</td>
                                <td>${re.getProfitPerMinute()}</td>
                                <td>${re.getOwner().getName()}<c:if test="${empty re.getOwner().getName()}">-</c:if></td>
                                <td><a href="<c:url value="/realestate/detail/${re.getId()}" />">editovat</a></td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</body>
</html>
